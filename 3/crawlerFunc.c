#include "crawlerHeader.h"

extern QueuePtr queue;
extern ProcessedListNodePtr list;

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-h\033[m host or IP\n\
\033[22;33m-p\033[m [int]  port\n\
\033[22;33m-c\033[m [int]  command port\n\
\033[22;33m-t\033[m [int > 0]  number of threads\n\
\033[22;33m-d\033[m  [filename]  save directory\n\
\033[22;33m-s\033[m  [path]  starting url\n", progName);
}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1;
    int serving_port = 0, command_port = 0, num_threads = 0;
    char save_dir[20];
    char host_or_ip[20];
    char starting_url[20];

    //initialize pinfo
    memset(pinfo->save_dir, '\0', sizeof(pinfo->save_dir));
    memset(pinfo->host_or_ip, '\0', sizeof(pinfo->host_or_ip));
    memset(pinfo->starting_url, '\0', sizeof(pinfo->starting_url));
    memset(save_dir, '\0', sizeof(save_dir));
    memset(host_or_ip, '\0', sizeof(host_or_ip));
    memset(starting_url, '\0', sizeof(starting_url));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'p':
                       argIter++;
                       serving_port = atoi(argv[argIter]);
                       break;
                  case 'c':
                        argIter++;
                        command_port = atoi(argv[argIter]);
                        break;
                  case 't':
                        argIter++;
                        num_threads = atoi(argv[argIter]);
                        break;
                  case 'd':
                       argIter++;
                       strcpy(save_dir, argv[argIter]);
                       break;
                  case 'h':
                       argIter++;
                       strcpy(host_or_ip, argv[argIter]);
                       break;
                 case 's':
                      argIter++;
                      strcpy(starting_url, argv[argIter]);
                      break;
                  default:
                      return EXIT_FAILURE;
              }
          }
          else{
            return EXIT_FAILURE;
          }
      }
      else{
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->save_dir, save_dir, sizeof(save_dir));
    memcpy(pinfo->host_or_ip, host_or_ip, sizeof(host_or_ip));
    memcpy(pinfo->starting_url, starting_url, sizeof(starting_url));
    // printf("fd %s\n", pinfo->fd);

    pinfo->serving_port = serving_port;
    pinfo->command_port = command_port;
    pinfo->num_threads = num_threads;

    if(strlen(pinfo->starting_url) == 0 || strlen(pinfo->save_dir) == 0 || strlen(pinfo->host_or_ip) == 0 || pinfo->serving_port == 0 || pinfo->command_port == 0 || pinfo->num_threads == 0){
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void perror_exit(char *message){
    perror(message);
    exit(EXIT_FAILURE);
}

int initializeQueue(char *starting_url){
  int i;
  // printf("Starting url is %s\n", starting_url);
  if ((queue = (QueuePtr)malloc(sizeof(Queue))) == NULL) {
		fprintf(stderr, "Queue malloc error !\n");
		return -1;
	}

  if ((queue->head = (QueueNodePtr)malloc(sizeof(QueueNode))) == NULL) {
		perror("Queue head malloc");
		return -1;
	}
  queue->tail = queue->head;
  // printf("Created head\n");
  if ((queue->head->data = (char *)malloc((MSG_BUF)*sizeof(char))) == NULL) {
    perror("Queue->data malloc");
    return -1;
  }
  memset(queue->head->data, '\0', sizeof(queue->head->data));
  strcpy(queue->head->data, starting_url); //STARTING URL
  queue->count = 1;
  queue->head->next =  NULL;

  return 0;
}

void printQueue(){
  int i;
  printf("Queue: count = %d\n", queue->count );
    QueueNodePtr current = queue->head;
    while(current != NULL){
      printf("%s\t", current->data);
      current = current->next;
    }
    printf("\n");
}


void searchQueue_Push(char *url){
  int exists_in_queue = 0;
  int exists_in_list;
  // printf("Going to searh if %s exists in queue\n", url);

  QueueNodePtr current = queue->head;
  while(current != NULL){
    if(strcmp(current->data, url) == 0){
      // printf("URL %s already exists (queue)\n", url );
      exists_in_queue = 1;
      break;
    }
    current = current->next;
  }
  if(exists_in_queue == 0){
    exists_in_list = searchList(url);
    if(exists_in_list == 0){
      // printf("New URL %s! Pushing in queue...\n", url);
      push(url);
    }
  }
}

int searchList(char *url){
  int exists_in_list = 0;
  ProcessedListNodePtr current = list;
  while(current != NULL){
    if(strcmp(current->proc_data, url) == 0){
      // printf("URL already exists (list)\n" );
      exists_in_list = 1;
      break;
    }
    current = current->next;
  }
  return exists_in_list;
}

void push(char *url){

  if ((queue->tail->next = (QueueNodePtr)malloc(sizeof(QueueNode))) == NULL) {
		perror("Queue head malloc");
		return;
	}
  if ((queue->tail->next->data = (char *)malloc((MSG_BUF)*sizeof(char))) == NULL) {
    perror("Queue->data malloc");
    return;
  }
  memset(queue->tail->next->data, '\0', sizeof(queue->tail->next->data));
  strcpy(queue->tail->next->data, url); //STARTING URL
  queue->tail->next->next = NULL;
  queue->count++;

  queue->tail = queue->tail->next;
}

void pop(){
  QueueNodePtr temp;
  temp = queue->head;
  queue->head = queue->head->next;
  free(temp->data);
  free(temp);
  queue->count--;
}

void pushList(char *url){
  ProcessedListNodePtr current = list;
  if(list == NULL){  //empty list
    if ((list = (ProcessedListNodePtr)malloc(sizeof(ProcessedListNode))) == NULL) {
    		fprintf(stderr, "ProcessedListNode malloc error !\n");
    		return;
    }
    if ((list->proc_data = (char *)malloc((MSG_BUF)*sizeof(char))) == NULL) {
        perror("list->data malloc");
        return;
    }
    memset(list->proc_data, '\0', sizeof(current->next->proc_data));
    strcpy(list->proc_data, url);
    list->next =  NULL;
  }
  else{
    while(current != NULL){
      if(current->next == NULL){
        if ((current->next = (ProcessedListNodePtr)malloc(sizeof(ProcessedListNode))) == NULL) {
          fprintf(stderr, "ProcessedListNode malloc error !\n");
          return;
        }
        if ((current->next->proc_data = (char *)malloc((MSG_BUF)*sizeof(char))) == NULL) {
            perror("list->data malloc");
            return;
        }
        memset(current->next->proc_data, '\0', sizeof(current->next->proc_data));
        strcpy(current->next->proc_data, url);
        current->next->next =  NULL;
        break;
      }
      current = current->next;
    }
  }
}

void printList(){
  int i;
  printf("List:\n");
  ProcessedListNodePtr current = list;
  while(current != NULL){
    printf("%s\t", current->proc_data);
    current = current->next;
  }
  printf("\n");
}

void freeList(){
  ProcessedListNodePtr temp;
  ProcessedListNodePtr current = list;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp->proc_data);
    free(temp);
  }
}
