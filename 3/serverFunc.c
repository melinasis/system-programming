#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "serverHeader.h"

extern int endOfExecution;
extern int started, working;
extern int served_pages, served_bytes;
extern pthread_mutex_t buffer_mtx;
extern pthread_cond_t empty_cv, full_cv;
extern BufferPtr buffer;
extern struct timeval start;

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-p\033[m [int]  serving port\n\
\033[22;33m-c\033[m [int]  command port\n\
\033[22;33m-t\033[m [int > 0]  number of threads\n\
\033[22;33m-d\033[m  [filename]  root directory\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1;
    int serving_port = 0, command_port = 0, num_threads = 0;
    char root_dir[20];

    //initialize pinfo
    memset(pinfo->root_dir, '\0', sizeof(pinfo->root_dir));

    memset(root_dir, '\0', sizeof(root_dir));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'p':
                       argIter++;
                       serving_port = atoi(argv[argIter]);
                       break;
                  case 'c':
                        argIter++;
                        command_port = atoi(argv[argIter]);
                        break;
                  case 't':
                        argIter++;
                        num_threads = atoi(argv[argIter]);
                        break;
                  case 'd':
                       argIter++;
                       strcpy(root_dir, argv[argIter]);
                       break;
                  default:
                      return EXIT_FAILURE;
              }
          }
          else{
            return EXIT_FAILURE;
          }
      }
      else{
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->root_dir, root_dir, sizeof(root_dir));
    // printf("fd %s\n", pinfo->fd);

    pinfo->serving_port = serving_port;
    pinfo->command_port = command_port;
    pinfo->num_threads = num_threads;

    if(strlen(pinfo->root_dir) == 0 || pinfo->serving_port == 0 || pinfo->command_port == 0 || pinfo->num_threads == 0){
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void freeBuffer(){
  int i;
  //free Queue
  QueueNodePtr current, temp;
  current = buffer->head;
  for(i = 0; i < (buffer->size); i++){
    free(current->data);
    temp = current;
    current = temp->next;
    free(temp);
  }
  free(buffer);
}

int initializeBuffer(int size){
  int i;
  if ((buffer = (BufferPtr)malloc(sizeof(Buffer))) == NULL) {
		fprintf(stderr, "Buffer malloc error !\n");
		return -1;
	}

  buffer->size = size;
  buffer->count = 0;

  if ((buffer->head = (QueueNodePtr)malloc(sizeof(QueueNode))) == NULL) {
		perror("Queue head malloc");
		return -1;
	}
  buffer->tail = buffer->head;
  // printf("Created head\n");
  if ((buffer->head->data = (char *)malloc((MSG_BUF*2)*sizeof(char))) == NULL) {
    perror("Queue->data malloc");
    return -1;
  }
  memset(buffer->head->data, '\0', sizeof(buffer->head->data));
  buffer->head->fd = -1;

  QueueNodePtr current = buffer->head;
  for(i = 1; i < (buffer->size); i++){
    // printf("Creating %d\n",i );
    if ((current->next = (QueueNodePtr)malloc(sizeof(QueueNode))) == NULL) {
  		perror("Queue head malloc");
  		return -1;
  	}
    current = current->next;
    if ((current->data = (char *)malloc((MSG_BUF*2)*sizeof(char))) == NULL) {
      perror("Queue->data malloc");
      return -1;
    }
    memset(current->data, '\0', sizeof(buffer->head->data));
    current->fd = -1;
  }
  current->next =  buffer->head;
  return 0;
}

void printBuffer(){
  int i;
  printf("BUFFER: size=%d count=%d\n",buffer->size, buffer->count );
    QueueNodePtr current = buffer->head;
    for(i = 0; i < (buffer->size); i++){
      printf("%d\t", current->fd);
      current = current->next;
    }
    printf("\n");
}

void *Thread(void * arg) {
  char *month[] ={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
  char *day[] ={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
  char *dir = (char *) arg;
  int sz, i;
  char *token1, *token2, *token3;
  char ch;
  int done = 0;
  int bytes_in;
  printf("\t\tThread %ld started\n", pthread_self());
  char *answerBuf, *body, *header;
  int valid;
  char *buf;

	time_t t;
	struct tm tm;

  char path[20];
  memset(path, '\0', sizeof(path));

  while (1) {
    if ((answerBuf = (char *)malloc(2*MSG_BUF*sizeof(char))) == NULL) {
      perror("answerBuf malloc");
    }
    memset(answerBuf, '\0', 2*MSG_BUF);
    if ((buf = (char *)malloc(MSG_BUF*sizeof(char))) == NULL) {
      perror("buf malloc");
    }
    memset(buf, '\0', MSG_BUF);
    if ((header = (char *)malloc(2*MSG_BUF*sizeof(char))) == NULL) {
      perror("header malloc");
    }
    memset(header, '\0', 2*MSG_BUF);
    pthread_mutex_lock(&buffer_mtx);

    //we check if buffer is empty
    while (buffer->count == 0 && endOfExecution == 0) {
      printf("\t\tFound buffer empty %ld\n", pthread_self());
      pthread_cond_wait(&empty_cv, &buffer_mtx);
      if (endOfExecution) {
        break;
      }
    }
    if (endOfExecution) {
      free(answerBuf);
      free(header);
      free(buf);
      pthread_mutex_unlock(&buffer_mtx);
      break;
    }
    //we can get things from the buffer

    printf("\t\t%ld: Buffer not empty gonna take something from it\n", pthread_self());
    valid = 1;
    bytes_in = read(buffer->head->fd, buf, MSG_BUF);
    printf("Got: (%d) %s\n", bytes_in, buf );
    buf[bytes_in-1] = '\0';

    if(strstr(buf, "Host:") == NULL){
      valid = 0;
    }

    //read message
    token1 = strtok(buf, " ");
    token2 = strtok(NULL, " ");
    token3 = strtok(NULL, " ");

    //check for bad request
    if(strcmp(token1, "GET") != 0){
      valid = 0;
    }
    if(token3 != NULL){
      if(strncmp(token3, "HTTP/1.1", 8) != 0){
        printf("in here\n" );
        valid = 0;
      }
    }
    else valid = 0;


    if( valid ){
      printf("\t\t\t%s working...\n",token1 );
      printf("\t\t\tGetting %s\n", token2);

      sprintf(path,"%s/%s", dir, token2);
      // printf("\t\t\tGoing to search for %s\n", path);
      FILE * fp;

      //file exists and can be read
      if((fp = fopen (path, "r")) != NULL){
        //200  OK
        fseek(fp, 0L, SEEK_END);
        sz = ftell(fp);
        rewind(fp);
        printf("\t\t\tSize of file is %d bytes\n", sz);
        if ((body = (char *)malloc(sz*sizeof(char))) == NULL) {
          perror("body malloc");
        }
        memset(body, '\0', sz*sizeof(char));
        //copy file contents to body
        fread (body, 1, sz, fp);
        fclose (fp);

        //construct header
        t = time(NULL);
        tm = *localtime(&t);
        sprintf(header, "HTTP/1.1 200 OK\nDate: %s, %02d %s %d %d:%02d:%02d GMT\nServer: myhttp/1.0.0 (Ubuntu 64)\nContent Length: %d\nContent-­Type: text/html\nConnection: Closed\n\n", day[tm.tm_wday], tm.tm_mday, month[tm.tm_mon], tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, sz);


        strcpy(answerBuf, header);
        // printf("\t%s\n", answerBuf);
        write(buffer->head->fd, answerBuf, 2*MSG_BUF); //send header

        memset(answerBuf, '\0', 2*MSG_BUF);
        if ((answerBuf = (char *)realloc(answerBuf, sz)) == NULL) {
          perror("answerBuf realloc");
        }

        write(buffer->head->fd, body, sz); //send body
        usleep(5000);
        served_pages++;
        served_bytes += sz;
        printf("\t\t\tPage served!\n");
        free(body);
      }
      else{
        if(errno == 2){
          t = time(NULL);
          tm = *localtime(&t);
          sprintf(header, "HTTP/1.1 400 Not Found\nDate: %s, %02d %s %d %d:%02d:%02d GMT\nServer: myhttp/1.0.0 (Ubuntu 64)\nContent Length: 124\nContent-­Type: text/html\nConnection: Closed\n\n<html>Sorry  dude,  couldn’t  find  this  file.</html>\n", day[tm.tm_wday], tm.tm_mday, month[tm.tm_mon], tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);

          strcpy(answerBuf, header);
          write(buffer->head->fd, answerBuf, 2*MSG_BUF); //send header
        }
        else if(errno == EACCES){
          t = time(NULL);
          tm = *localtime(&t);
          sprintf(header, "HTTP/1.1 403 Forbidden\nDate: %s, %02d %s %d %d:%02d:%02d GMT\nServer: myhttp/1.0.0 (Ubuntu 64)\nContent Length: 124\nContent-­Type: text/html\nConnection: Closed\n\n<html>Trying  to  access  this  file  but  don’t  think  I  can  make  it.</html> \n", day[tm.tm_wday], tm.tm_mday, month[tm.tm_mon], tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);

          strcpy(answerBuf, header);
          write(buffer->head->fd, answerBuf, 2*MSG_BUF); //send header
        }
      }
    }
    else{
      sprintf(header, "HTTP/1.1 400 Bad Request\n");
      strcpy(answerBuf, header);
      // printf("\t%s\n", answerBuf);
      write(buffer->head->fd, answerBuf, 2*MSG_BUF); //send header

    }

    // printf("buf: %s\n", buf);
    // printf("Worker %ld read : %s\n", pthread_self(), buffer->data[buffer->count-1]);

    close(buffer->head->fd);

    // memset(buffer->data[buffer->count-1], '\0', 2*MSG_BUF);
    buffer->count--;
    buffer->head = buffer->head->next;
    //unlocking the mutex! we are done
    pthread_cond_broadcast(&full_cv);
    pthread_mutex_unlock(&buffer_mtx);

    // getFileFromContentServer(answerBuf, dir);
    free(answerBuf);
    free(header);
    free(buf);
  }

  // free((int *) arg);
  printf("\t\tExiting thread %ld ...\n", pthread_self());
  pthread_exit(NULL); //valgrind returned leaked for this!
  //return NULL;
}

void placeInBuffer(int readSock) {
  pthread_mutex_lock(&buffer_mtx);
  //we check if buffer is full
	while (buffer->count == buffer->size) {
		printf("\tFound buffer full\n");
		pthread_cond_wait(&full_cv, &buffer_mtx);
	}
  printf("\tGonna place it in %d\n", buffer->count);
	//we can place things in the buffer
	// sprintf(buffer->data[buffer->count], "%s,%s,%d,%d", dn->cont, ServerAdd[contID], ServerPort[contID], contID);
  buffer->tail->fd = readSock;
  buffer->tail = buffer->tail->next;
  buffer->count++;

	//unlocking the mutex! we're done with the CS here
	pthread_cond_broadcast(&empty_cv);
	pthread_mutex_unlock(&buffer_mtx);

}

void perror_exit(char *message) {
    perror(message);
    exit(EXIT_FAILURE);
}

void commandPortFunction(int fd){
    char *answerBuf;
    int bytes_in, i;
    char buf[MSG_BUF];
    double sec_elapsedTime, ms_elapsedTime;
    struct timeval end;
    time_t curtime;
    char buffer_time[30];
    char temp[30];

    if ((answerBuf = (char *)malloc(2*MSG_BUF*sizeof(char))) == NULL) {
      perror("answerBuf malloc");
    }

    memset(answerBuf, '\0', 2*MSG_BUF);
    memset(temp, '\0',sizeof(temp));

    bytes_in = read(fd, buf, MSG_BUF);

    printf("\t\tReading fd: %d -> (%d) ", fd, bytes_in);
    for(i = 0; i < bytes_in; i++){
      printf("%c", buf[i] );
    }
    printf("\n");

    if(strncmp(buf, "STATS", 5) == 0){
      gettimeofday(&end, NULL);
      curtime = end.tv_sec;
      strftime(buffer_time, 30, "%T.",localtime(&curtime));
      // printf("Ending time %s%ld\n", buffer_time, start.tv_usec);

      int microseconds = (end.tv_sec - start.tv_sec) * 1000000 + ((int)end.tv_usec - (int)start.tv_usec);
      int milliseconds = microseconds/1000;

      struct timeval tv3;

      tv3.tv_sec = microseconds/1000000;
      int minutes = tv3.tv_sec/SEC_PER_MIN;
      int hours = minutes/SEC_PER_HOUR;
      tv3.tv_sec = tv3.tv_sec%SEC_PER_MIN;
      tv3.tv_usec = microseconds%1000000;

      sprintf(temp, "Server up for ");
      strcat(answerBuf, temp);
      memset(temp, '\0',sizeof(temp));

      if(hours > 0){
        sprintf(temp, "%d:", hours);
        strcat(answerBuf, temp);
        memset(temp, '\0',sizeof(temp));
      }
      if(minutes > 0){
        sprintf(temp, "%d:", minutes);
        strcat(answerBuf, temp);
        memset(temp, '\0',sizeof(temp));
      }

      sprintf(temp, "%ld.%ld", tv3.tv_sec, tv3.tv_usec);
      strcat(answerBuf, temp);
      memset(temp, '\0',sizeof(temp));

      sprintf(temp, ", served %d pages %d bytes \n", served_pages, served_bytes);
      strcat(answerBuf, temp);
      memset(temp, '\0',sizeof(temp));

      // printf("%s\n", answerBuf);
      write(fd, answerBuf, 2*MSG_BUF);
    }
    else if(strncmp(buf, "SHUTDOWN", 8) == 0){
      working = 0;
      printf("Going to terminate now!\n");
    }
    else{
      strcat(answerBuf, "Command not recognised\n");
      write(fd, answerBuf, 2*MSG_BUF);
    }

    free(answerBuf);
}
