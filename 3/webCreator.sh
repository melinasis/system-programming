#!/bin/bash
if [ "$#" -ne 4 ]
	then echo "Missing a mandatory parameter"
	exit 1
fi

DIR=$1
FILE=$2
w=$3
p=$4
re='^[0-9]+$'

#1
if ! [[ -d $DIR ]]; then
  echo Folder $DIR does not exist.
  exit 1
fi

#2
if ! [[ -f $FILE ]]; then
  echo File $FILE does not exist.
  exit 1
fi

#3
if ! [[ $w =~ $re ]] ; then
   echo Error: $w is not an integer.
   exit 1
fi

#4
if ! [[ $p =~ $re ]] ; then
   echo Error: $p is not an integer.
   exit 1
fi

#check file size
lines=($(wc -l < $FILE))
if [[ $lines < 10000 ]]; then
    echo $FILE has less than 10000 lines.
    exit 1
fi

#chech if root_directory is full
if ! [ -z "$(ls -A $DIR)" ]; then
   echo "Warning: directory is full, purging ..."
	 rm -rf $DIR
	 mkdir $DIR

fi

#page numbers: create array with random numbers
total_pages=$(($w * $p))
random_numbers=($(shuf -e $(seq 1 $total_pages)))
random_pages=()
for (( a = 0; a < $total_pages ; a++ ))
do
 let "b=a/w"
	random_pages+=(../site$b/page$b\_${random_numbers[a]}.html )
done
# echo "${random_numbers[@]}"
# echo "${random_pages[@]}"
temp='0'
index='0'
sum='0'


#create sites
cd $DIR
let "maxd=w-1"

for i in `seq 0 $maxd`
do
	echo "Creating web site $i ..."
  mkdir "site$i"
  cd "site$i"

	#create pages
  for j in `seq 1 $p`
  do

		names_q=()
		k=$(shuf -i 1-$((lines-2000)) -n 1)
		m=$(shuf -i 1000-2000 -n 1)
		# echo m = $m

		#inside links
		inside_links_1=(${random_numbers[@]:$index:$p})
		inside_links=("${inside_links_1[@]}")
		let "nf=(p/2)+1"

		if [[ ${#inside_links[@]} -gt  $nf ]]; then
			unset inside_links[temp]
		fi

		#outer links
		let "nq=(w/2)+1"
		outer_links=("${random_numbers[@]}")

		for (( ind = 0; ind < $total_pages ; ind++ ))
		do
			flag='0'
			#find index
			for del in "${inside_links_1[@]}"
			do
				# echo del = $del
				# echo ${outer_links[ind]} == $del
				if [[ ${outer_links[ind]} -eq $del ]]; then
					# echo $ind:delete ${outer_links[ind]}
					unset outer_links[ind]
					flag='1'
				fi
			done
			if [[ $flag -eq 0 ]]; then
				# echo $ind:adding ${random_pages[ind]}
				names_q+=(${random_pages[ind]})
			fi
		done

		# echo inside = ${inside_links[@]}
		# echo outer = ${outer_links[@]}

		f=($(shuf -n $nf -e ${inside_links[@]}))
		q=($(shuf -n $nq -e ${names_q[@]}))
		# echo f = ${f[@]}
		# echo q = ${q[@]}

		names_f=()
		for a in "${f[@]}"
		do
			names_f+=( page$i\_$a.html )
		done

		# echo f: ${names_f[@]}
		# echo q: ${q[@]}

		linksTotal=("${names_f[@]}" "${q[@]}")
		# echo total: ${linksTotal[@]}
		shufLinksTotal=($(shuf -e ${linksTotal[@]}))
		# echo shuf: ${shufLinksTotal[@]}

	  let "copy_lines=(m/(nf+nq))"

		let "set_size = (copy_lines/(nf+nq))"
		let "sets = nf+nq"
		s='1'
		# echo  sets = $sets set_size = $set_size

		printf "\t"
		echo  Creating page $DIR/site$i/page$i\_${random_numbers[$temp]}.html with $copy_lines lines starting at line $k ...
    touch page$i\_${random_numbers[$temp]}.html

		echo -e "<!DOCTYPE html>\n<html>\n\t<body>\n" >> page$i\_${random_numbers[$temp]}.html
		cd ../..

		let "end_line=k+copy_lines-1"
		for print_line in `seq $k $end_line`
		do

			awk NR==$print_line $FILE >> $DIR/site$i/page$i\_${random_numbers[$temp]}.html
			((sum++))
			if [ $sum -eq $set_size ] && [ $s -lt $sets ]; then
				# echo sum:$sum set_size:$set_size , s:$s sets:$sets

				echo -e "<a href=${shufLinksTotal[s-1]}>link$s</a>" >> $DIR/site$i/page$i\_${random_numbers[$temp]}.html
				printf "\t\t"
				echo Adding link to ${shufLinksTotal[s-1]}
				sum='0'
				((s++))
			fi

			if [ $print_line -eq $end_line ]; then
				echo -e "<a href=${shufLinksTotal[s-1]}>link$s</a>" >> $DIR/site$i/page$i\_${random_numbers[$temp]}.html
				printf "\t\t"
				echo Adding link to ${shufLinksTotal[s-1]}
				sum='0'
			fi

		done
		cd $DIR/site$i
		echo -e "\t</body>\n</html>" >> page$i\_${random_numbers[$temp]}.html

		((temp++)) #random page numbers array

		s='1'

  done
  cd .. #back to root directory
	let "index = index+p"

done
