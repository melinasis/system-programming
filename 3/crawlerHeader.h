#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/select.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <dirent.h>
#define perror2(s,e) fprintf(stderr, "%s: %s\n", s, strerror(e))

#define MSG_BUF 256

typedef struct Prog* Pinfo;
typedef struct QueueNode* QueueNodePtr;
typedef struct Queue* QueuePtr;
typedef struct ProcessedListNode* ProcessedListNodePtr;

typedef struct Prog{
	int serving_port, command_port, num_threads;
	char save_dir[20];
  char host_or_ip[20];
	char starting_url[20];
}Prog;

typedef struct Queue{
	QueueNodePtr head, tail;
	int count;
}Queue;

typedef struct QueueNode{
	QueueNodePtr next;
	char *data;
}QueueNode;

typedef struct ProcessedListNode{
	ProcessedListNodePtr next;
	char *proc_data;
}ProcessedListNode;

void perror_exit(char *message);
int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
int initializeQueue(char *starting_url);
void printQueue();
void searchQueue_Push(char *url);
int searchList(char *url);
void push(char *url);
void pop();
void pushList(char *url);
void printList();
void freeList();
