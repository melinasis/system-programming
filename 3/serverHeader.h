#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <pthread.h>
#include <math.h>
#include <dirent.h>
#include <sys/time.h>
#include <time.h>

#define perror2(s,e) fprintf(stderr, "%s: %s\n", s, strerror(e))

#define SEC_PER_DAY   86400
#define SEC_PER_HOUR  3600
#define SEC_PER_MIN   60
#define MSG_BUF 256

typedef struct Prog* Pinfo;
typedef struct QueueNode* QueueNodePtr;
typedef struct Buffer* BufferPtr;

typedef struct Prog{
	int serving_port, command_port, num_threads;
	char root_dir[20];
}Prog;

typedef struct Buffer{
	int size;
	QueueNodePtr head, tail;
	int count;
}Buffer;

typedef struct QueueNode{
	QueueNodePtr next;
	int fd;
	char *data;
}QueueNode;

int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
void *Thread(void *arg);
void placeInBuffer(int readSock);
void terminate(int sig);
int initializeBuffer(int size);
int sigemptyset(sigset_t *set);
int sigaddset(sigset_t *set, int signum);
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
void child_server(int readSock);
void perror_exit(char *message);
void sigchld_handler (int sig);
void printBuffer();
void commandPortFunction(int fd);
void freeBuffer();
