Προγραμματισμός Συστήματος
3η Εργασία

Μελίνα Σίσκου 1115201200161

Για compile εντολή make.

CREATOR:	./webCreator.sh web_sites text_file.txt w p
SERVER:		./myhttpd -p serving_port -c command_port -t num_threads -d web_sites/
CRAWLER:	./mycrawler -h localhost -p serving_port -c command_port -t num_threads -d folder -s starting url (!προσθήκη -s flag)

SERVER
!Έχει υλοποιηθεί όλη η λειτουργία του server.

Δημιουργούνται 2 sockets (serving και command) και o server κάνει listen και στα δύο.
Χρησιμοποιείται η SELECT για επιλογή μεταξύ των δύο.

Serving Port - Λειτουργία thread:
read buf-> Δεκτά αιτήματα της μορφής: GET page_path HTTP/1.1 Host: (επιπρόσθετες πληροφορίες δεκτές - αγνοούνται)
write header-> Στέλνει πρώτα το header (200, 400, 404, 403)
write body-> Στη συνέχεια στέλνει το body

CRAWLER
!Ο crawler δημιουργεί πιστό αντίγραφο του webCreator χωρίς όμως την υλοποίηση threads.
!Δεν έχει υλοποιηθεί το command port του crawler

Αφού δημιουργηθεί μια ουρά με μόνο ένα κόμβο (starting url)
επαναληπτικά, όσο η ουρά έχει στοιχεία γίνεται get request με το url της κεφαλής
διαβάζεται το header της απάντησης
αν ληφθεί η απάντηση 200, διαβάζεται και το body και αντιγράφεται το αρχείο

!μορφή url: site/page.html

στη συνέχεια κάθε link μέσα στο αρχείο εισάγεται μέσα στο αρχείο
1)αν δεν υπάρχει ήδη μέσα στην ουρά && 2)αν δεν έχει ήδη επεξεργαστεί(ProcessedList)

στο τέλος γίνεται pop από την ουρά
