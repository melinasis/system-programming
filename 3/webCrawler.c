#include "crawlerHeader.h"
QueuePtr queue;
ProcessedListNodePtr list;

int main(int argc, char *argv[]) {
  struct stat st = {0};

    int i, j, body_size;
    char *line = NULL;
    char *ret;
    size_t len = 0;
    ssize_t read_bytes;
    char *pch, *readAnswerBuf, *body;
    char *token, *page, *website;
    char buf[256];


    list = NULL;
    //
    char *requestGET;
    char url[100];
    memset(url, '\0', sizeof(url));
    //

    Pinfo pinfo = malloc(sizeof(Prog));
    if(pinfo == NULL){
          printf("Error! memory not allocated.");
          return EXIT_FAILURE;
    }

    if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
    		printHelp(argv[0]);
    		return EXIT_FAILURE;
    }

    // printf("Serving port: %d\n", pinfo->serving_port);
    // printf("Command port: %d\n", pinfo->command_port);
    // printf("Number of threads: %d\n", pinfo->num_threads);
    // printf("Save directory: %s\n", pinfo->save_dir);
    // printf("Host: %s\n", pinfo->host_or_ip);
    // printf("Starting Url: %s\n", pinfo->starting_url);

    int threads = pinfo->num_threads;
    int serving_port = pinfo->serving_port;
    int command_port = pinfo->command_port;

    int sock;

    mkdir(pinfo->save_dir, 0777);

    struct sockaddr_in server;
    struct sockaddr *serverptr = (struct sockaddr*)&server;
    struct hostent *rem;

    initializeQueue(pinfo->starting_url);
    // printQueue();

    while(queue->count > 0){
      /* Create socket */
      if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      perror_exit("socket");
      /* Find server address */
      if ((rem = gethostbyname(pinfo->host_or_ip)) == NULL) {
        herror("gethostbyname"); exit(1);
      }

      server.sin_family = AF_INET;       /* Internet domain */
      memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
      server.sin_port = htons(serving_port);         /* Server port */
      /* Initiate connection */
      if (connect(sock, serverptr, sizeof(server)) < 0)
      perror_exit("connect");
      printf("Connecting to %s port %d\n", pinfo->host_or_ip, serving_port);


      // printf("Give input string: ");
      // read_bytes = getline(&line, &len, stdin);
      // line[read_bytes] = '\0';
      //create valid GET request
      if ((requestGET = (char *)malloc(2*MSG_BUF*sizeof(char))) == NULL) {
        perror("requestGET malloc");
      }
      memset(requestGET, '\0', 2*MSG_BUF);
      sprintf(requestGET, "GET %s HTTP/1.1 Host:", queue->head->data );
      printf("%s\n",requestGET );


      if (write(sock, requestGET, 2*MSG_BUF) < 0){
        perror_exit("write");
      }

      strtok(requestGET, " ");
      website = strtok(NULL, "/");
      page = strtok(NULL, " ");
      // printf("website = %s\n", website);
      // printf("page = %s\n", page);

      if ((readAnswerBuf = (char *)malloc(2*MSG_BUF*sizeof(char))) == NULL) {
        perror("readAnswerBuf malloc");
      }
      memset(readAnswerBuf, '\0', 2*MSG_BUF);

      //read header
      if (read(sock, readAnswerBuf,  2*MSG_BUF) < 0){
        perror_exit("read");
      }

      // printf("Received bytes: %ld\n", strlen(readAnswerBuf));
      printf("\nReceived header: \n%s\n", readAnswerBuf);

      //find length of body
      token = strtok(readAnswerBuf, " ");
      //token: HTTP/1.1
      token = strtok(NULL, " ");
      //token: number
      // printf("token is %s\n", token);
      if(atoi(token) == 200){
        token = strtok(NULL, "\n");
        token = strtok(NULL, "\n");
        // printf("token is %s\n", token);
        //token: Date
        token = strtok(NULL, "\n");
        // printf("token is %s\n", token);
        //token: Server
        token = strtok(NULL, "\n");
        //token: Content Length
        token = strtok(token, " ");
        token = strtok(NULL, " ");
        token = strtok(NULL, "\n");
        // printf("token is %s\n", token);
        body_size = atoi(token);

        if ((body = (char *)malloc(body_size*sizeof(char))) == NULL) {
          perror("body malloc");
        }
        memset(body, '\0', body_size);


        //read body
        if (read(sock, body, body_size) < 0){
          perror_exit("read");
        }
        // printf("Received body\nBytes: %d\n", body_size);

        chdir(pinfo->save_dir);
        mkdir(website, 0777);
        chdir(website);
        //create file
        FILE *fp = fopen(page, "w");
        fprintf(fp,"%s", body);
        fclose(fp);
        free(body);

        fp = fopen(page, "r");
        while (getline(&line, &len, fp) != -1) {
          if((ret = strstr(line, "href")) != NULL){
            // printf("%s\n", ret);
            strtok(ret, "=");
            token = strtok(NULL, ">");
            // printf("%s\n", token);
            //correct Url
            if(token[0] == '.'){ //outer link
              token = strtok(token, "/");
              token = strtok(NULL, " ");
              // printf("fixed: %s\n", token);
              searchQueue_Push(token);
            }
            else{ //inner link
              sprintf(url, "%s/%s", website, token);
              // printf("fixed: %s\n", url);
              searchQueue_Push(url);
            }
          }
        }
        fclose(fp);
        chdir("../..");
        pushList(queue->head->data);
        pop();
        // printQueue();
        // printList();
        // printf("Received BODY string: \n%s\n", readAnswerBuf);
      }
      else{
        printf("BAD REQUEST\n");
      }

      close(sock);                 /* Close socket and exit */
      free(requestGET);
      free(readAnswerBuf);
    }

    freeList();
    free(queue);
    free(pinfo);
    free(line);
}
