
#include "serverHeader.h"

#define SIZE 10 // fixed size of the buffer.

int endOfExecution = 0;  // variable that signals the threads to stop execution.
int started = 0, working = 1; // variables to declare the state of the server.
int served_pages = 0, served_bytes = 0;
pthread_mutex_t buffer_mtx;
pthread_cond_t empty_cv, full_cv;
BufferPtr buffer; // the fixed buffer
struct timeval start;

int main(int argc, char *argv[]){
  int i, j;
  int err;
  time_t curtime;
  char buffer_time[30];

  pthread_t *threadPool; // array that hold the threads.

  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }

  // printf("Serving port: %d\n", pinfo->serving_port);
  // printf("Command port: %d\n", pinfo->command_port);
  // printf("Number of threads: %d\n", pinfo->num_threads);
  // printf("Root directory: %s\n", pinfo->root_dir);
  int threads = pinfo->num_threads;
  int servingPort = pinfo->serving_port;
  int commandPort = pinfo->command_port;

  /* Initialize mutex and condition variables*/
	pthread_mutex_init(&buffer_mtx, NULL);
	pthread_cond_init(&empty_cv, NULL);
	pthread_cond_init(&full_cv, NULL);


  int sock1, sock2, readSock, newSock;


	struct sockaddr_in server, client;
	socklen_t clientlen, serverlen;
	struct sockaddr *serverptr=(struct sockaddr *)&server;
	struct sockaddr *clientptr=(struct sockaddr *)&client;
	struct hostent *rem;


	/* Create socket 1*/
	if ((sock1 = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror_exit("socket");
		return 1;
	}
	server.sin_family = AF_INET;       /* Internet domain */
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(servingPort);      /* The given port */
	/* Bind socket to serving port*/
	if (bind(sock1, serverptr, sizeof(server)) < 0) {
		perror_exit("bind");
	}

  /* Create socket 2*/
	if ((sock2 = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror_exit("socket");
		return 1;
	}

	/* Bind socket to command port*/
  server.sin_port = htons(commandPort);
	if (bind(sock2, serverptr, sizeof(server)) < 0) {
		perror_exit("bind");
	}

	/* Listen for connections */
	if (listen(sock1, 128) < 0) {
		perror_exit("listen");
	}
	printf("Listening for connections to port %d\n", servingPort);
  if (listen(sock2, 128) < 0) {
		perror_exit("listen");
	}
	printf("Listening for connections to port %d\n", commandPort);


  if (initializeBuffer(SIZE) < 0) {
		return -1;
	}
  // printBuffer();
  printf("Creating %d threads\n", threads);
	if ((threadPool = (pthread_t *)malloc(threads * sizeof(pthread_t))) == NULL) {
		perror_exit("threadPool malloc");
	}
	for (int i = 0; i < threads; i++) {
		//Creating Threads here.
		if (err = pthread_create(threadPool+i, NULL, Thread, (void *) pinfo->root_dir)) {
			perror2("pthread_create", err);
			return 1;
		}
	}

  //set of socket descriptors
  fd_set read_fds;
  int retval;

  clientlen = sizeof(client);
  printf("Server at your service! Waiting for connections ...\n");
  gettimeofday(&start, NULL);
  curtime = start.tv_sec;
  strftime(buffer_time,30,"%T.",localtime(&curtime));
  // printf("Starting time %s%ld\n", buffer_time, start.tv_usec);

  while (working) {
    //clear the socket set
    FD_ZERO(&read_fds);
    //add sock1 to set
    FD_SET(sock1, &read_fds);
    //add sock2 to set
    FD_SET(sock2, &read_fds);

    //SELECT
    //wait for an activity on one of the sockets , timeout is NULL , so wait indefinitely
    retval = select(sock2 + 1, &read_fds, NULL, NULL, NULL);
    if ( retval == -1 ) {
      if (errno == EINTR) {
        continue;
      }
      perror("select error");
      return -1;
    }
    else if ( retval ) {
      if (FD_ISSET(sock1, &read_fds)) { //reading from serving port
        if ((readSock = accept(sock1, clientptr, &clientlen)) < 0) perror_exit("accept");
        printf("\nAccepted connection (serving port)\n");
        placeInBuffer(readSock);
      }
      else{
        if (FD_ISSET( sock2 , &read_fds)){ //reading from command port
          if ((newSock = accept(sock2, clientptr, &clientlen)) < 0) perror_exit("accept");
          printf("\nAccepted connection (command port)\n");
          commandPortFunction(newSock);
          close(newSock);
        }
      }
    }
  }
  // The server has been signaled to stop its execution.
  close(sock1);
  close(sock2);
  endOfExecution = 1;
  pthread_cond_broadcast(&empty_cv);
  printf("Waiting for threads to terminate\n");
	for (int i = 0; i < threads; i++) {
		if ((err = pthread_join(*(threadPool+i), NULL))) {
			perror2("pthread_join", err);
		  exit(1);
		}
  }

  if (err = pthread_mutex_destroy(&buffer_mtx)) {
        perror2("pthread_mutex_destroy1", err);
		// return 1;
	}
    if (err = pthread_cond_destroy(&empty_cv)) {
        perror2("pthread_cond_destroy2", err);
		// return 1;
	}
	if (err = pthread_cond_destroy(&full_cv)) {
        perror2("pthread_cond_destroy3", err);
		return 1;
	}


  freeBuffer();

  free(threadPool);
  free(pinfo);
  printf("\n\nIts been a pleasure working with you ! Goodbye\n");
  return 0;
}
