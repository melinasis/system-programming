#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "prog.h"
#include "trie.h"
#include "postingList.h"
#include "functions.h"
#include "workers.h"

extern int totalWords;
extern int mapSize;
extern trieNodePtr trie;

void dfWord(trieNodePtr root, char* word){
  postingListNodePtr postingList = searchWord(root, word);
  if(postingList != NULL){
    //word exists
    printf("%s", word);
    printPostingListSize(postingList, 0, NULL);
  }
  else{
    printf("\t\033[22;31mThe word you are searching for does not exist\033[m\n");
  }
}

void dfAll(trieNodePtr root, Pinfo pinfo){
  printPostingLists(root, 1, pinfo);
}

int max_tf(trieNodePtr root, char *word, char *max_path, char *max_file){
  int max = 0;
  postingListNodePtr postingList = searchWord(root, word);
  if(postingList != NULL){
    //word exists
    //search postingList for this id
    postingListNodePtr current = postingList;
    max = current->no;
    strcpy(max_path, current->path);
    strcpy(max_file, current->fileName);
    while(current != NULL){
        if(current->no > max){
          max = current->no;
          memset(max_path, '\0', sizeof(max_path));
          memset(max_file, '\0', sizeof(max_file));
          strcpy(max_path, current->path);
          strcpy(max_file, current->fileName);
        }
        // printf("%s: %s/%s (%d)\n", word, current->path, current->fileName, current->no);
        current = current->next;
    }
    // printf("%d: %s/%s\n", max, max_path, max_file);
    return max;
  }
  else{
    // printf("\033[22;31mThe word you are searching for does not exist\033[m\n");
    return -1;
  }
}

int min_tf(trieNodePtr root, char *word, char *min_path, char *min_file){
  int min = 0;
  postingListNodePtr postingList = searchWord(root, word);
  if(postingList != NULL){
    //word exists
    //search postingList for this id
    postingListNodePtr current = postingList;
    min = current->no;
    strcpy(min_path, current->path);
    strcpy(min_file, current->fileName);
    while(current != NULL){
        if(current->no < min){
          min = current->no;
          memset(min_path, '\0', sizeof(min_path));
          memset(min_file, '\0', sizeof(min_file));
          strcpy(min_path, current->path);
          strcpy(min_file, current->fileName);
        }
        // printf("%s: %s/%s (%d)\n", word, current->path, current->fileName, current->no);
        current = current->next;
    }
    // printf("%d: %s/%s\n", min, min_path, min_file);
    return min;
  }
  else{
    // printf("\033[22;31mThe word you are searching for does not exist\033[m\n");
    return -1;
  }
}

postingListNodePtr searchWord(trieNodePtr root, char* word){
  int j = 0;
  // printf("%s ready to be searched\n", word);
  return searchDown(trie, word, j);
}

postingListNodePtr searchDown(trieNodePtr current, char* word, int j){
  char ch;
  ch = word[j];
  // printf("%c\n", ch);
  if(current->headChild->myChild == NULL){
    // printf("No children\n");
    //no children
    return NULL;
  }
  else{
    // printf("this trieNode has children\n" );
    //this trieNode has children
    trieNodePtr exists = existsInList(current, ch);
    if(exists != NULL){
      //This character already exists
      j++;
      if(word[j] == '\0'){
        // printf("This is the end of the word\n" );
        //Did you find it?
        if(exists->myPostingList != NULL){
          // printf("Found!!!\n" );
          return exists->myPostingList;
        }
        else{
          return NULL;
        }
      }
      searchDown(exists, word, j);
    }
    else{
      // printf("This character doesn't exist\n" );
      return NULL;
    }
  }

}

void search(trieNodePtr root, nodePtr searchList, docPtr* docAr, answerPtr answerList, char* log_buf){
  int i, df, old_id, new_id;
  int at_least_one = 0;
  float old_score, new_score;
  int done = -1, counter = 1;;
  idNodePtr cur2;
  idNodePtr idList = malloc(sizeof(idNode));
  idList->id = -1;
  idList->score = 0;
  idList->flag = 0;
  idList->next = NULL;

  nodePtr currentWord = searchList;


  char ttime[7], date[9];
	memset(ttime, '\0', sizeof(ttime));
	memset(date, '\0', sizeof(date));
	time_t t = time(NULL);
	struct tm tm;
  tm = *localtime(&t);
  sprintf(ttime, "%d:%d:%d", tm.tm_hour, tm.tm_min, tm.tm_sec);
  sprintf(date, "%d/%d/%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
  memset(log_buf, '\0', MSG_BUF);

  while(currentWord != NULL){
    // printf("current word %s\n", currentWord->word);
    postingListNodePtr postingList = searchWord(root, currentWord->word);

    if(postingList != NULL){
      at_least_one = 1;
      //word exists
      // printf("%s found\n", currentWord->word);
      strcat(log_buf, date);
      strcat(log_buf, " ");
      strcat(log_buf, ttime);
      strcat(log_buf, ": search: ");
      strcat(log_buf, currentWord->word);
      strcat(log_buf,": ");
      df = postingListSize(postingList);
      postingListNodePtr current = postingList;
      while(current != NULL){
        // printf("in doc %d\n", current->id);
        strcat(log_buf, current->path);
        strcat(log_buf,"/");
        strcat(log_buf, current->fileName);
        strcat(log_buf,": ");
        pushIdList(idList, current->id, current->no, docAr, df);
        pushAnswerList(answerList, current->id, current->no, current->path, current->fileName, docAr);
        current = current->next;
      }
      strcat(log_buf, "\n");
    }
    else{
      // printf("%s does not exist\n", currentWord->word);
    }
    //next word
    currentWord = currentWord->next;
  }

  if(at_least_one == 0){
    answerList->f = -1;
    free(idList);
    // printf("\t%d: \033[22;31mNo results found\033[m\n", getpid());
    return;
  }
  deleteIdList(idList);
}

void pushIdList(idNodePtr idList, int id, int no, docPtr* docAr, int df){
  idNodePtr current = idList;

  //Calculate score for this word and id
  int tf = no;
  float k1 = 1.2;
  float b = 0.75;
  int D = docAr[id]->wordCount;
  float avgdl = totalWords/mapSize;
  double idf = log((totalWords - df + 0.5) / (df+0.5));
  float score = idf * ( (tf*(k1+1)) / (tf+k1*(1-b+b*(D/avgdl))) );
  // printf("score = %f\n", score);

  if(idList->id == -1){
    idList->id = id;
    idList->score += score;
  }
  else{
    while(current != NULL){
      if(current->id == id){
        current->score += score;
        return;
        //no need to enter the same id
      }
      if(current->next != NULL){
        current = current->next;
      }
      else  break;
    }
    //current is last
    // printf("%d id hasn't been entered before\n", id);
    current->next = malloc(sizeof(idNode));
    current->next->id = id;
    current->next->score = score;
    current->next->flag = 0;
    current->next->next = NULL;
  }
}

int idListSize(idNodePtr idList){
  int size = 0;
  idNodePtr current = idList;
  while(current != NULL){
    size++;
    current = current->next;
  }
  return size;
}

void deleteIdList(idNodePtr head){
  idNodePtr temp;
  idNodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
