#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct postingListNode* postingListNodePtr;
typedef struct tempNode* tempPtr;

typedef struct postingListNode{
  int id;
  char path[200];
  char fileName[100];
  int no;
  postingListNodePtr next;
}postingListNode;

typedef struct tempNode{
  char value[1];
  tempPtr next;
  tempPtr prev;
}tempNode;

postingListNodePtr insertPostingList(postingListNodePtr head, int id);
void printPostingLists(trieNodePtr root, int function, Pinfo pinfo);
void printDown(childNodePtr currentChild, tempPtr tempList, int function, Pinfo pinfo);
tempPtr createTempList();
void pushTempList(tempPtr tempList, char ch);
void printTempList(tempPtr tempList, int function, Pinfo pinfo);
void deleteLast(tempPtr tempList);
void printPostingList(postingListNodePtr head);
void printPostingListSize(postingListNodePtr head, int function, Pinfo pinfo);
int postingListSize(postingListNodePtr head);
void deletePostingList(postingListNodePtr head);
void deleteChildrenList(childNodePtr head);
