#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog.h"
// #include "trie.h"
// #include "functions.h"

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-d\033[m [filename]  input docfile\n\
\033[22;33m-w\033[m  [int]  preferred number of workers\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1;
    int k = 10; //default
    int w = 0;
    char docfile[20];

    //initialize pinfo
    memset(pinfo->docfile,'\0',sizeof(pinfo->docfile));
    pinfo->k = k;

    memset(docfile,'\0',sizeof(docfile));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'd':
                       argIter++;
                       strcpy(docfile, argv[argIter]);
                       break;
                  case 'w':
                       argIter++;
                       w = atoi(argv[argIter]);
                       // if(w == 0){
                       //   w = 5; //if 0 is given make k default
                       // }
                       break;
                  default:
                      return EXIT_FAILURE;
              }
          }
          else{
            return EXIT_FAILURE;
          }
      }
      else{
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->docfile, docfile, sizeof(docfile));
    // printf("fd %s\n", pinfo->fd);

    pinfo->k = k;
    pinfo->w = w;

    if(strlen(pinfo->docfile) == 0 || w == 0){

      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void firstReadDocfile(Pinfo pinfo){
  int numDir = 0;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *pch;

  stream = fopen(pinfo->docfile, "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    numDir++;
  }
  pinfo->numDir = numDir;

  // printf("Dirs are %d\n", pinfo->numDir);

  fclose(stream);
  free(line);

}

void secondReadDocfile(Pinfo pinfo, dirPtr* dirAr){
  int position = 0;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->docfile, "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char* pch;

      dirPtr dirP = malloc(sizeof(Dir));
      if(dirP == NULL){
            printf("Error! memory not allocated.");
            return;
      }

      memset(dirP->path,'\0',sizeof(dirP->path));
      strcpy(dirP->path, line);
      //finished reading path
      dirAr[position] = dirP;   //saved to array
      position++;
  }
  fclose(stream);
  free(line);
}

void deleteSearchList(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}

nodePtr createWordList(){
  nodePtr wordList = malloc(sizeof(node));
  if(wordList == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  memset(wordList->word,'\0',sizeof(wordList->word));
  strcpy(wordList->word," ");
  wordList->next = NULL;
  return wordList;
}

void push(nodePtr head, char* word) {
    nodePtr current = head;
    if(strcmp(head->word," ") == 0){    //first node
      memset(head->word,'\0',sizeof(head->word));
      strcpy(head->word, word);
    }
    else{
      while (current->next != NULL) {
          current = current->next;
      }
      /* now we can add two nodes */
      current->next = malloc(sizeof(node));
      if(current->next == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      memset(current->next->word,'\0',sizeof(current->next->word));
      strcpy(current->next->word, word);
      current->next->next = NULL;
    }

}

void freeWordList(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}

nodePtr nextNode(nodePtr current){
  return current->next;
}

void printList_forSearch(nodePtr head, nodePtr searchList){
  nodePtr current = head;
  nodePtr currentWord;

  while(current != NULL){
    currentWord = searchList;

    while(currentWord != NULL){
      if(strcmp(currentWord->word, current->word) == 0){
        printf("\033[22;32m%s \033[m", current->word);
        break;
      }
      currentWord = currentWord->next;
    }
    if(currentWord == NULL){
      printf("%s ", current->word);
    }

    current = current->next;
  }
  printf("\n\n");
  // printf("Number of nodes: %d\n", countNode(head));
}
