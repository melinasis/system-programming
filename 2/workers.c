#include "workers.h"
#include "prog.h"
#include "trie.h"
#include "postingList.h"
#include "functions.h"

extern int *worker_pids, *worker_read_fds, *worker_write_fds, *worker_maps, *worker_tries;
extern int numOfWorkers;
extern FILE* fp;
extern docPtr* map;
extern trieNodePtr trie;
extern int mapSize;
int term = 0;

void createWorkerList(WorkerList **list) {
	(*list) = (WorkerList *)malloc(sizeof(WorkerList));
	(*list)->first = NULL;
	(*list)->last = NULL;
	(*list)->numOfWorkers = 0;
	(*list)->activeWorkers = 0;
}

void addWorkerNodeToList(WorkerList *list, int i, int pid, int started, int dir_first, int dir_last){

	WorkerNode *node;

	node = (WorkerNode*)malloc(sizeof(WorkerNode));
	node->pid = pid;
	node->i = i;
	node->status = 0;
	node->started = started;
  node->dir_first = dir_first;
  node->dir_last = dir_last;
	node->next = NULL;

	if (list->first == NULL) { //if list is empty.
		list->first = node;
		list->last = node;
	}
	else { //if list contains at least one node.
		list->last->next = node;
		list->last = node;
	}
}

void freeWorkerList(WorkerList *list){
	WorkerNode *aux;

	while(list->first != NULL){
		aux = list->first->next;
		free(list->first);
		list->first = aux;
		if (aux != NULL) {
			aux = aux->next;
		}
	}
	free(list);
}

void createWorkerFifos(int numOfWorkers, int **jExecutor_read_fds, int **jExecutor_write_fds, int **worker_read_fds, int **worker_write_fds){
  if (((*jExecutor_read_fds) = (int *)malloc(numOfWorkers*sizeof(int))) == NULL) {
		free(jExecutor_read_fds);
		perror("Error allocating memory");
		return;
	}
  if (((*worker_write_fds) = (int *)malloc(numOfWorkers*sizeof(int))) == NULL) {
		free(worker_write_fds);
		perror("Error allocating memory");
		return;
	}
  if (((*worker_read_fds) = (int *)malloc(numOfWorkers*sizeof(int))) == NULL) {
		free(worker_read_fds);
		perror("Error allocating memory");
		return;
	}
  if (((*jExecutor_write_fds) = (int *)malloc(numOfWorkers*sizeof(int))) == NULL) {
		free(jExecutor_write_fds);
		perror("Error allocating memory");
		return;
	}


  char fifoOUT[6], fifoIN[5];
  for (int i = 0; i < numOfWorkers; i++){
    memset(fifoOUT, '\0', sizeof(fifoOUT));
  	memset(fifoIN, '\0', sizeof(fifoIN));

  	sprintf(fifoIN, "%dIN", i);// JOB EXECUTOR : read from, WORKER : write to.
  	sprintf(fifoOUT, "%dOUT", i);// JOB EXECUTOR : write to, WORKER : read from.

  	if ((mkfifo(fifoIN, 0666) == -1) && (errno != EEXIST)){
  		perror("mkfifo error!");
  		return;
  	}
  	if ((mkfifo(fifoOUT, 0666) == -1) && (errno != EEXIST)){
  		perror("mkfifo error!");
  		return;
  	}

  	//first i open the fifoIN pipe from both ends so both job executor and worker can have access to.
  	// JOB EXECUTOR : read from, WORKER : write to.

    if (((*jExecutor_read_fds)[i] = open(fifoIN, O_RDONLY | O_NONBLOCK)) < 0) {
  		perror("Error ! Can't open worker_fifoIN!");
  		return;
  	}
  	if (((*worker_write_fds)[i] = open(fifoIN, O_WRONLY | O_NONBLOCK)) < 0) {
  		perror("Error ! Can't open worker_fifoIN!");
  		return;
  	}

  	//then i open the fifoOUT pipe from both ends for the above reason.
  	// JOB EXECUTOR : write to, WORKER : read from.
  	if (((*worker_read_fds)[i] = open(fifoOUT, O_RDONLY | O_NONBLOCK)) < 0) {
  		perror("Error ! Can't open worker_fifoOUT!");
  		return;
  	}
  	if (((*jExecutor_write_fds)[i] = open(fifoOUT, O_WRONLY | O_NONBLOCK)) < 0) {
  		perror("Error ! Can't open worker_fifoOUT!");
  		return;
  	}
  }
}

docPtr* createMap(int a, int b, dirPtr* dirAr, int* size){
	int cur = a;
	int mapSize;
	int numDocs = 0;
	int mapPosition = 0;
	docPtr* docAr;

	mapSize = findMapSize(dirAr, cur, b);

	// printf("MAPSIZE = %d\n", mapSize);
	//allocate memory for map
	docAr = malloc(mapSize*sizeof(docPtr));

	char cwd[1024];
  getcwd(cwd, sizeof(cwd));
  // printf("Current working dir: %s\n", cwd);

	while(cur <= b){
		scanDir(dirAr, cur, docAr, cwd, &mapPosition);
		cur++;
	}

	*size = mapSize;
	return docAr;
}

void readAsciiFile(FILE *stream, docPtr* docAr, int* position, char *path, char *filename){
	int wordCount = 0;
	char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *pch;

	docPtr docP = malloc(sizeof(Doc));
	if(docP == NULL){
				printf("Error! memory not allocated.");
	}

	docP->wordList = createWordList();
	docP->lines = 0;
	docP->bytes = 0;
	strcpy(docP->path, path);
	strcpy(docP->fileName, filename);
	while ((read = getline(&line, &len, stream)) != -1) {
	 	 line[read - 1] = 0;
	 	 char* pch;
	 	 // strcpy(docP->path, path);
	 	 // strcpy(docP->fileName, filename);

	 	 pch = strtok(line, " ");
	 	 while(pch != NULL){
	 			 push(docP->wordList, pch);
	 			 wordCount++;
	 			 pch = strtok(NULL, " ");
	 	 }

		 docP->bytes += read;
		 docP->lines++;
	}
	docP->wordCount = wordCount;
	docP->bytes--;
	//finished reading ascii file
	docAr[*position] = docP;   //saved to array
	free(line);
}

void workerFunction(int read_fd, int write_fd, int mapSize, docPtr* map, FILE* fp, char *folderName){
	char read_buf[MSG_BUF], write_buf[MSG_BUF], log_buf[MSG_BUF];
	char* pch;
	nodePtr currentWord;
	answerPtr cur1;
	char max_path[200], min_path[200];
  char max_file[100], min_file[100];
	memset(max_path, '\0', sizeof(max_path));
	memset(max_file, '\0', sizeof(max_file));
	memset(min_path, '\0', sizeof(min_path));
	memset(min_file, '\0', sizeof(min_file));
	memset(log_buf, '\0', MSG_BUF);
	memset(read_buf, '\0', MSG_BUF);
	memset(write_buf, '\0', MSG_BUF);
	fd_set read_fds;
	int retval, bytes_in, wordCount, lines, bytes, i, tf;

	char ttime[7], date[9];
	memset(ttime, '\0', sizeof(ttime));
	memset(date, '\0', sizeof(date));
	time_t t = time(NULL);
	struct tm tm;

	struct sigaction shut;
	sigset_t shut_mask;

	sigemptyset(&shut_mask);
	sigaddset(&shut_mask, SIGCHLD);

	memset(&shut, 0, sizeof(shut));
	shut.sa_handler = shut_handler;
	shut.sa_mask = shut_mask;
	sigaction(SIGTERM, &shut, NULL);

	FD_ZERO(&read_fds);
	FD_SET(read_fd, &read_fds);

	while(1){
		retval = select(read_fd +1, &read_fds, NULL, NULL, NULL);
		if ( retval == -1 ) {
			if (errno == EINTR) { // if a signal interrupted the select call
				// continue;
			}
			perror("\tselect");
			return;
		}
		else if ( retval ) {
			bytes_in = read(read_fd, read_buf, MSG_BUF);
			if (!bytes_in) { // if read returns zero
				// break;
			}
			read_buf[bytes_in] = '\0';

			printf("\tWorker %d received query : %s\n", getpid(), read_buf);
			tm = *localtime(&t);
			sprintf(ttime, "%d:%d:%d", tm.tm_hour, tm.tm_min, tm.tm_sec);
			sprintf(date, "%d/%d/%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);

			//answer
			pch = strtok(read_buf, " ");

			if(strcmp(pch, "/search") == 0){

				//first word
				pch = strtok(NULL, " ");
	      nodePtr searchList = malloc(sizeof(node));
	      strcpy(searchList->word, pch);
	      searchList->next = NULL;
	      int sum = 1;

				//continue creating searchList
	      nodePtr current = searchList;
	      while((pch = strtok(NULL, " ")) != NULL && sum < 10){
	          current->next = malloc(sizeof(node));
	          current = current->next;
	          strcpy(current->word, pch);
	          current->next = NULL;
	          sum++;
	      }

	      //print
	      // current = searchList;
	      // while(current != NULL){
	      //   printf("%s -> ", current->word);
	      //   current = current -> next;
	      // }printf("\n");

				answerPtr answerList = malloc(sizeof(Answer));
				answerList->id = -1;
				answerList->f = -1;
			  answerList->next = NULL;
	      search(trie, searchList, map, answerList, log_buf);
				// printf("%s\n",log_buf );
				chdir(folderName);
				fprintf(fp,"%s", log_buf);
				chdir("..");
				// printf("ANSWER IS %d\n", answerList->f);
				// print Answer List
				if(answerList->f == 1){
					sprintf(write_buf, "SEARCH 1\n");
					cur1 = answerList;
				  while(cur1 != NULL){
				    // printf("\t%d: %s/%s ", getpid(), cur1->path, cur1->fileName);
						strcat(write_buf, cur1->path);
						strcat(write_buf, "/");
						strcat(write_buf, cur1->fileName);
						strcat(write_buf, " ");
						printAnswerWordlist(cur1, searchList, write_buf);

						// printf("\n" );
						strcat(write_buf, "\n");

				    cur1 = cur1->next;
				  }//printf("\n" );
				}
				else{
					sprintf(write_buf, "SEARCH 0\n");
				}
				deleteSearchList(searchList);
				freeAnswerList(answerList);
			//
			}
			else if(strcmp(pch, "/wc") == 0){
				chdir(folderName);
				fprintf(fp,"%s %s: wc:\n", date, ttime);
				chdir("..");
				wordCount = 0;
				lines = 0;
				bytes = 0;
				for(i = 0; i < mapSize; i++){
					wordCount = wordCount + map[i]->wordCount;
					lines = lines + map[i]->lines;
					bytes = bytes + map[i]->bytes;
				}
				sprintf(write_buf, "WC %d %d %d", wordCount, lines, bytes);
			}
			else if(strcmp(pch, "/maxcount") == 0){
				pch = strtok(NULL, " ");
				if((tf = max_tf(trie, pch, max_path, max_file)) != -1){
					// printf("max is %d: %s/%s\n", tf, max_path, max_file);
					sprintf(write_buf, "MAXCOUNT %s/%s %d",  max_path, max_file, tf);
					chdir(folderName);
					fprintf(fp,"%s %s: maxcount: %s: %s/%s (%d)\n", date, ttime, pch, max_path, max_file, tf);
					chdir("..");
				}
				else{
					sprintf(write_buf, "MAXCOUNT / %d", -1);
				}
			}
			else if(strcmp(pch, "/mincount") == 0){
				pch = strtok(NULL, " ");
				if((tf = min_tf(trie, pch, min_path, min_file)) != -1){
					// printf("min is %d: %s/%s\n", tf, min_path, min_file);
					sprintf(write_buf, "MINCOUNT %s/%s %d",  min_path, min_file, tf);
					chdir(folderName);
					fprintf(fp,"%s %s: mincount: %s: %s/%s (%d)\n", date, ttime, pch, min_path, min_file, tf);
					chdir("..");
				}
				else{
					sprintf(write_buf, "MINCOUNT / %d", -1);
				}
			}
			write(write_fd, write_buf, MSG_BUF);
		}
	}
}

int findMapSize(dirPtr* dirAr, int cur, int b){
	int mapSize = 0;
	struct dirent **namelist;
	int n, i;

	while(cur <= b){
		n = scandir(dirAr[cur]->path, &namelist, NULL, alphasort);
		if (n == -1) {
				perror("scandir");
		}
		mapSize =  mapSize + n - 2;
		cur++;

		for(i = 0; i < n; i++){
			free(namelist[i]);
		}
		free(namelist);
	}
	return mapSize;
}

void scanDir(dirPtr* dirAr, int cur, docPtr* docAr, char* cwd, int* mapPosition){
	struct dirent **namelist;
	int n;
	FILE *stream;

	// printf("\t%d: Scan %s\n", getpid(), dirAr[cur]->path);
	n = scandir(dirAr[cur]->path, &namelist, NULL, alphasort);

	// printf("\t\tfiles = %d\n", n-2);

	 if (n == -1) {
			 perror("scandir");
	 }

	 chdir(dirAr[cur]->path);

	 n--;
	 while(n > 1){
		 if((stream = fopen(namelist[n]->d_name, "r" )) == NULL){
			 perror("fopen");
		 }
		 else{
			 // printf("\t%d: Opening %s \n", getpid(), namelist[n]->d_name);
		 }
		 readAsciiFile(stream, docAr, mapPosition, dirAr[cur]->path, namelist[n]->d_name);
		 (*mapPosition)++;
		 fclose(stream);
		 free(namelist[n]);
		 n--;
	 }
	 free(namelist[0]);
	 free(namelist[1]);
	 free(namelist);

	 chdir(cwd);

}

void shut_handler(int sig){
	int i;
	printf("\treceived SIGTERM from job executor\n");
	for(i = 0; i < mapSize; i++){
		freeWordList(map[i]->wordList);
		free(map[i]);
	}
	freeTrie(trie);
	free(map);
	fclose(fp);
	term = 1;
}


void pushAnswerList(answerPtr answerList, int id, int no, char *path, char *fileName, docPtr* docAr){
  answerPtr current = answerList;

  if(answerList->id == -1){
    answerList->id = id;
		answerList->f = 1;
		answerList->wordList = docAr[id]->wordList;
		strcpy(answerList->path, path);
		strcpy(answerList->fileName, fileName);
  }
  else{
    while(current != NULL){
      if(current->id == id){
        return;
        //no need to enter the same id
      }
      if(current->next != NULL){
        current = current->next;
      }
      else  break;
    }
    //current is last
    // printf("%d id hasn't been entered before\n", id);
    current->next = malloc(sizeof(Answer));
    current->next->id = id;
		current->next->f = 1;
		strcpy(current->next->path, path);
		strcpy(current->next->fileName, fileName);
		current->next->wordList = docAr[id]->wordList;
    current->next->next = NULL;
  }
}

void printAnswerWordlist(answerPtr answer, nodePtr searchList, char* write_buf){
	nodePtr current = answer->wordList;
	nodePtr currentWord;
	char* colored;

	while(current != NULL){
    currentWord = searchList;

    while(currentWord != NULL){
      if(strcmp(currentWord->word, current->word) == 0){
        // printf("\033[22;32m%s \033[m", current->word);
				strcat(write_buf, current->word);
				strcat(write_buf," ");
        break;
      }
      currentWord = currentWord->next;
    }
    if(currentWord == NULL){
      // printf("%s ", current->word);
			strcat(write_buf, current->word);
			strcat(write_buf, " ");
    }
    current = current->next;
  }
}

void freeAnswerList(answerPtr head){
  answerPtr temp;
  answerPtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
