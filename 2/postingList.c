#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "trie.h"
#include "postingList.h"

extern docPtr* map;

postingListNodePtr insertPostingList(postingListNodePtr head, int id){
  if(head == NULL){
    //first time this word appears no posting list exists yet
    head = malloc(sizeof(postingListNode));
    head->next = NULL;
    head->id = id;
    strcpy(head->path, map[id]->path);
    strcpy(head->fileName, map[id]->fileName);
    head->no = 1;
    // printf("Posting list created\n" );
    return head;
  }
  else{
    //check doc id
    postingListNodePtr current = head;
    while(current != NULL){
      if(current->id == id){
        // printf("This word already exists in this document\n");
        current->no++;
        return head;
      }
      if(current->next == NULL){
        // printf("This word exists in a different doc\n");
        current->next =  malloc(sizeof(postingListNode));
        current->next->next = NULL;
        current->next->id = id;
        strcpy(current->next->path, map[id]->path);
        strcpy(current->next->fileName, map[id]->fileName);
        current->next->no = 1;

        current = current->next;
        return head;
      }
      current = current->next;
    }
  }
}

void printPostingList(postingListNodePtr head){
  printf(" -> " );
  postingListNodePtr current = head;
  while(current != NULL){
    printf("[%s, %d, %s] -> ", current->path, current->no, current->fileName);
    current= current->next;
  }
  printf("\n");
}

void printPostingListSize(postingListNodePtr head, int function, Pinfo pinfo){
  int size = 0;
  postingListNodePtr current = head;
  while(current != NULL){
    size++;
    current= current->next;
  }
  printf(" %d\n", size);
}

int postingListSize(postingListNodePtr head){
  int size = 0;
  postingListNodePtr current = head;
  while(current != NULL){
    size++;
    current= current->next;
  }
  return size;
}

void printPostingLists(trieNodePtr root, int function, Pinfo pinfo){
  tempPtr tempList;
  trieNodePtr currentTrieNode = root;
  childNodePtr currentChild = root->headChild;
  // printf("\nPOSTING LISTS\n");
  tempList = createTempList();
  while(currentChild != NULL) {
    printDown(currentChild, tempList, function, pinfo);
    currentChild = currentChild ->next;
  }
  free(tempList);
}

void printDown(childNodePtr currentChild, tempPtr tempList, int function, Pinfo pinfo){
  trieNodePtr currentTrieNode = currentChild->myChild;
  printf("%c", currentTrieNode->value[0]);

  if(currentTrieNode->myPostingList != NULL){
    if(function ==  1){
      //df
      printPostingListSize(currentTrieNode->myPostingList, function, pinfo);
    }
    else{
      printPostingList(currentTrieNode->myPostingList);
    }
    if(currentTrieNode->headChild->myChild != NULL){
      //There is more
      pushTempList(tempList, currentTrieNode->value[0]);
      printTempList(tempList, function, pinfo);
    }
    else{
      return; //go up
    }
  }
  else{
    pushTempList(tempList, currentTrieNode->value[0]);
  }

  currentChild = currentTrieNode->headChild;
  while(currentChild != NULL) {
    printDown(currentChild, tempList, function, pinfo);
    currentChild = currentChild ->next;
    if(currentChild == NULL){
      deleteLast(tempList);
    }
    else{
      //not just previous - ola ta prothemata
      // printf("\n");
      printTempList(tempList, function, pinfo);
    }
  }
}

//temp List to help print prefixes
tempPtr createTempList(){
  tempPtr temp = malloc(sizeof(tempNode));
  memset(temp->value, '\0', sizeof(temp->value));
  strcpy(temp->value,  " ");
  temp->next = NULL;
  temp->prev = NULL;
  return temp;
}

void pushTempList(tempPtr tempList, char ch){
  tempPtr current;
  if(strcmp(tempList->value, " ") == 0){
    memset(tempList->value, '\0', sizeof(tempList->value));
    tempList->value[0] = ch;
  }
  else{
    current = tempList;
    while(current->next != NULL){
      current = current->next;
    }
    current->next = malloc(sizeof(tempNode));
    memset(current->next->value, '\0', sizeof(current->value));
    current->next->value[0] = ch;
    current->next->next = NULL;
    current->next->prev = current;
  }
}

void printTempList(tempPtr tempList, int function, Pinfo pinfo){
  tempPtr current = tempList;
  while(current != NULL){
    printf("%c", current->value[0]);
    current = current->next;
  }
}

void deleteLast(tempPtr tempList){
  tempPtr current = tempList;
  while(current->next != NULL){
    current = current->next;
  }
  //current is last
  if(current->prev != NULL){
    current->prev->next = NULL;
    free(current);
  }
  else{
    //only one node left
    memset(current->value, '\0', sizeof(current->value));
    strcpy(current->value,  " ");
    current->next = NULL;
    current->prev = NULL;
  }
}

void deletePostingList(postingListNodePtr head){
  postingListNodePtr temp;
  postingListNodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
