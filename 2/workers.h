#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>
#define _GNU_SOURCE
#define MSG_BUF 4096

typedef struct Prog* Pinfo;
typedef struct Dir* dirPtr;
typedef struct Doc* docPtr;
typedef struct Answer* answerPtr;
typedef struct node* nodePtr;

typedef struct WorkerNode{
  int pid;
  int i;
  int status; //status values : 0 is for Active, 1 is for Finished, 2 is for Suspended.
  int started;
  int dir_first;
  int dir_last;
  struct WorkerNode *next;
}WorkerNode;

typedef struct WorkerList {
	int numOfWorkers;
	int activeWorkers;
	struct WorkerNode *first;
	struct WorkerNode *last;
}WorkerList;

typedef struct Answer{
  int id;
	char path[200];
  char fileName[100];
  int f;
  nodePtr wordList;
  answerPtr next;
}Answer;

void createWorkerList(WorkerList **list);
void addWorkerNodeToList(WorkerList *list, int i, int pid, int started, int dir_first, int dir_last);
void freeWorkerList(WorkerList *list);
void createWorkerFifos(int numOfWorkers, int **jExecutor_read_fds, int **jExecutor_write_fds, int **worker_read_fds, int **worker_write_fds);
docPtr* createMap(int a, int b, dirPtr* dirAr, int* size);
void readAsciiFile(FILE *stream, docPtr* docAr, int* position, char *path, char *filename);
void workerFunction(int read_fd, int write_fd, int mapSize, docPtr* map, FILE* fp, char *folderName);
int findMapSize(dirPtr* dirAr, int cur, int b);
void worker_handler(int sig);
void scanDir(dirPtr* dirAr, int cur, docPtr* docAr, char* cwd, int* mapPosition);
void shut_handler(int sig);
void pushAnswerList(answerPtr answerList, int id, int no, char *path, char *fileName, docPtr* docAr);
void printAnswerWordlist(answerPtr answer, nodePtr searchList, char* write_buf);
void freeAnswerList(answerPtr head);
