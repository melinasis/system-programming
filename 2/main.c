#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "workers.h"
#include "trie.h"
#include "postingList.h"

int *worker_pids = NULL, *worker_read_fds = NULL, *worker_write_fds = NULL, *worker_maps = NULL, *worker_tries = NULL;
FILE* fp;
docPtr* map;
trieNodePtr trie;
int mapSize, totalWords;

int main(int argc, char *argv[]){
  int i, j;
  WorkerList *wlist;
  char read_buf[MSG_BUF], write_buf[MSG_BUF], *dir; // buffer for the queries
  char complete_search_answer[MSG_BUF];
  int finished = 0;
  int *jExecutor_read_fds = NULL, *jExecutor_write_fds = NULL, max_fd;
  char *end;
  char *pch;
  int wc, search;

  memset(complete_search_answer, '\0', MSG_BUF);
  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }

  firstReadDocfile(pinfo);
  if(pinfo->w > pinfo->numDir){
    pinfo->w = pinfo->numDir;
  }
  int numOfWorkers = pinfo->w;
  int numDir = pinfo->numDir;
  printf("%d directories\n", numDir);
  printf("%d workers\n", numOfWorkers);

  //distribute directories
  int cur = 0;
  int first_dir, last_dir;
  int a = numDir / numOfWorkers;
  int b = numDir % numOfWorkers;
  // printf("a = %d\n", a);
  // printf("b = %d\n", b);

  //dirAr holds all paths from input
  dirPtr* dirAr = malloc(numDir*sizeof(Dir));
  if(dirAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  secondReadDocfile(pinfo, dirAr);

  // print
  // for(i = 0; i < numDir; i++){
  //   printf("%s\n", dirAr[i]->path);
  // }


  createWorkerList(&wlist); //A list that contains all workers that are going to be created.
  createWorkerFifos(numOfWorkers, &jExecutor_read_fds, &jExecutor_write_fds, &worker_read_fds, &worker_write_fds);
  char folderName[50];
  struct stat st = {0};
  sprintf(folderName, "log");
  if(stat(folderName, &st) == -1 ) {
    if (mkdir(folderName, 0700) != 0) {
      perror("log mkdir Error!");
    }
  }

  WorkerNode *current;
  char fileName[50];
  int test = 0;
  int status;
  pid_t childpid;
  pid_t mypid;
  pid_t wpid;

  if ((worker_pids = (int *)malloc(numOfWorkers*sizeof(int))) == NULL) {
    free(worker_pids);
    perror("Error allocating memory");
    return -1;
  }

  //create-start workers
  for (i = 0; i < numOfWorkers; i++){
    childpid = fork();
    if(childpid == -1){
      perror("worker fork error!");
      return -1;
    }
    else if(childpid == 0){ //child

      if(i == numOfWorkers -1)  a = a+b;
      printf("i %d: I am the child process %ld parent ID:%ld child ID:%ld, from %d to %d\n", i, (long)getpid(), (long)getppid(), (long)childpid, cur, cur+a-1);

      test = 1;
      map = createMap(cur, cur+a-1, dirAr, &mapSize);
      totalWords = 0;
      for(j = 0; j < mapSize; j++){
        totalWords += map[j]->wordCount;
      }
      trie = createTrie();
      insertTrie(trie, map, pinfo);
      // printPostingLists(trie, 0, pinfo);

      sprintf(fileName, "Worker_%d", getpid());
      chdir(folderName);
      fp = fopen(fileName, "w");
      chdir("..");
      // for(j = 0; j < mapSize; j++){
      //   printf("%d: wc=%d lines=%d bytes=%d\n", j, map[j]->wordCount, map[j]->lines, map[j]->bytes);
      // }
      break;
    }

    //parent does this
    if(i == numOfWorkers -1)  a = a+b;
    addWorkerNodeToList(wlist, i, childpid, 0, cur, cur+a-1);
    cur = cur+a;
    worker_pids[i] = childpid;

  }

//begin communications
  if(childpid != 0){ // job coordinator
    printf("i %d: I am the parent process %ld parent ID:%ld child ID:%ld\n", i, (long)getpid(), (long)getppid(), (long)childpid);
    // while ((wpid = wait(&status)) > 0); /// this way, the father waits for all the child processes
    char* str;
    char *line = NULL;
    size_t len = 0;
    ssize_t rd;

    char input[150];
    memset(input, '\0', sizeof(input));
    int wordCount_Sum = 0, wc = 0, search = 0, maxcount = 0, mincount = 0, totalMin = 0, totalMax = 0, lines_Sum = 0, bytes_Sum = 0;
    int max_v, min_v;
    char max_p[200], totalMax_p[200];
    char min_p[100], totalMin_p[100];
    int flag = 1;
    fd_set read_fds;
  	struct timeval tv;
  	int retval, bytes_in;
    int read_fd = 0; //stdin

  	FD_ZERO(&read_fds);
  	FD_SET(read_fd, &read_fds);
  	max_fd = read_fd;

    while (1) {
      //select: stdin(send query) or worker fifos(receive answer)
  		retval = select(max_fd + 1, &read_fds, NULL, NULL, NULL);
  		if ( retval == -1 ) {
  			if (errno == EINTR) {
  				continue;
  			}
  			perror("select error");
  			return -1;
  		}
  		else if ( retval ) {

  			if (FD_ISSET(read_fd, &read_fds)) { //reading from stdin
          if ((rd = getline(&line, &len, stdin)) != -1){
            //check for correct input and write to fifo
            line[rd - 1] = 0;
            strcpy(input, line);
            pch = strtok(line, " ");

            if(rd == 1){
              printf("\n");
            }
            else if(strcmp(pch, "/search") == 0){
              pch = strtok(NULL, " ");
              if(pch == NULL){
                printf("\033[22;31mMissing word to search. Try again\033[m\n" );
                continue;
              }
              for(i = 0 ; i < numOfWorkers; i++){
                //send query to all workers
                write(jExecutor_write_fds[i], input, strlen(input));
              }
            }
            else if(strcmp(pch, "/wc") == 0){
              for(i = 0 ; i < numOfWorkers; i++){
                //send query to all workers
                write(jExecutor_write_fds[i], pch, strlen(input));
              }
            }
            else if(strcmp(pch, "/maxcount") == 0){
              pch = strtok(NULL, " ");
              if(pch == NULL){
                printf("\033[22;31mMissing word to search. Try again\033[m\n" );
                continue;
              }
              for(i = 0 ; i < numOfWorkers; i++){
                //send query to all workers
                write(jExecutor_write_fds[i], input, strlen(input));
              }
            }
            else if(strcmp(pch, "/mincount") == 0){
              pch = strtok(NULL, " ");
              if(pch == NULL){
                printf("\033[22;31mMissing word to search. Try again\033[m\n" );
                continue;
              }
              for(i = 0 ; i < numOfWorkers; i++){
                //send query to all workers
                write(jExecutor_write_fds[i], input, strlen(input));
              }
            }
            else if(strcmp(pch, "/exit") == 0){
              for (int i = 0; i < numOfWorkers; i++) {
    						if (worker_pids[i] != -1) {
    							int status;
    							printf("\tSending SIGTERM to %d\n", worker_pids[i]);
    							kill(worker_pids[i], SIGTERM);
    							waitpid(worker_pids[i], &status, 0);
    						}
    					}
              printf("GOODBYE!\n");
              free(line);
              finished = 1;
              break;
            }
            else{
              printf("\033[22;31mCommand doesn't exist. Try again.\033[m\n" );
            }
          }
        }
        else{
          //in this 'for' the JobExecutor reads from the pipes the programme has between its workers and the Executor
          for (int i = 0; i < numOfWorkers; i++) {
            if (FD_ISSET(jExecutor_read_fds[i], &read_fds)) {
              char *token;
              while((bytes_in = read(jExecutor_read_fds[i], read_buf, MSG_BUF)) > 0) {
                // printf("RESULTS: %s\n",read_buf );
                pch = strtok(read_buf, " ");
                if(strcmp(pch, "WC") == 0){
                  wc++;
                  pch = strtok(NULL, " ");
                  wordCount_Sum += atoi(pch);
                  pch = strtok(NULL, " ");
                  lines_Sum += atoi(pch);
                  pch = strtok(NULL, " ");
                  bytes_Sum += atoi(pch);
                }
                else if(strcmp(pch, "SEARCH") == 0){
                  pch = strtok(NULL, "\n");
                  if(strcmp(pch, "1") == 0){
                    while((pch = strtok(NULL, "\n")) != NULL){
                      strcat(complete_search_answer,pch);
                      strcat(complete_search_answer,"\n");
                      // printf("\t%s\n",pch);
                    }
                  }
                  search++;
                }
                else if(strcmp(pch, "MAXCOUNT") == 0){
                  maxcount++;
                  pch = strtok(NULL, " ");
                  strcpy(max_p, pch);
                  pch = strtok(NULL, " ");
                  max_v = atoi(pch);
                  if(max_v != -1){
                    if(max_v > totalMax){
                      totalMax = max_v;
                      memset(totalMax_p, '\0', sizeof(totalMax_p));
                      strcpy(totalMax_p, max_p);
                    }
                  }}
                  else if(strcmp(pch, "MINCOUNT") == 0){
                    mincount++;
                    pch = strtok(NULL, " ");
                    strcpy(min_p, pch);
                    pch = strtok(NULL, " ");
                    min_v = atoi(pch);
                    if(min_v != -1){
                      if(flag == 1){
                        totalMin = min_v;
                        memset(totalMin_p, '\0', sizeof(totalMin_p));
                        strcpy(totalMin_p, min_p);
                        flag = 0;
                      }
                      if(min_v < totalMin){
                        totalMin = min_v;
                        memset(totalMin_p, '\0', sizeof(totalMin_p));
                        strcpy(totalMin_p, min_p);
                        printf("!!!!!!!!!!!!!!!!%s\n", totalMin_p);
                      }
                    }}}}}
          // if(wordCount_Sum != 0)  printf("WC ALL: %d words\n", wordCount_Sum);
        }
        if(wc == numOfWorkers){
          printf("\nAll workers done with wc\n" );
          printf("------------------------------------------------------------------------------------------------------------------------\n" );
          printf("Total:\twordcount\033[22;31m %d\033[m\n", wordCount_Sum);
          printf("\tlines\033[22;31m %d\033[m\n", lines_Sum);
          printf("\tbytes\033[22;31m %d\033[m\n", bytes_Sum);
          printf("------------------------------------------------------------------------------------------------------------------------\n\n" );
          wc = 0;
          wordCount_Sum = 0;
          lines_Sum = 0;
          bytes_Sum = 0;
        }
        if(search == numOfWorkers){
          //wait for all workers to send answer
          printf("\nAll workers done with search\n" );
          printf("------------------------------------------------------------------------------------------------------------------------\n" );
          printf("%s",complete_search_answer );
          printf("------------------------------------------------------------------------------------------------------------------------\n\n" );
          search = 0;
          memset(complete_search_answer, '\0', MSG_BUF);
        }
        if(maxcount == numOfWorkers){
          //wait for all workers to send answer
          printf("\nAll workers done with maxcount\n" );
          printf("------------------------------------------------------------------------------------------------------------------------\n" );
          printf("%s (%d)\n", totalMax_p, totalMax);
          printf("------------------------------------------------------------------------------------------------------------------------\n\n" );
          maxcount = 0;
          totalMax = 0;
          memset(max_p, '\0', sizeof(max_p));
        }
        if(mincount == numOfWorkers){
          //wait for all workers to send answer
          printf("\nAll workers done with mincount\n" );
          printf("------------------------------------------------------------------------------------------------------------------------\n" );
          printf("%s (%d)\n", totalMin_p, totalMin);
          printf("------------------------------------------------------------------------------------------------------------------------\n\n" );
          mincount = 0;
          totalMin = 0;
          flag = 1;
          memset(min_p, '\0', sizeof(min_p));
        }
      }

      if(finished == 1) break;
      FD_ZERO(&read_fds);
  		FD_SET(read_fd, &read_fds);
  		max_fd = read_fd;
  		for (int i = 0; i < numOfWorkers; i++) {
  			FD_SET(jExecutor_read_fds[i], &read_fds);
  			if (jExecutor_read_fds[i] > max_fd) {
  				max_fd = jExecutor_read_fds[i];
  			}
  		}
    }

  }
  else if(childpid == 0){ //worker
    //find i-find fifo
    current = wlist->first;
    while(current != NULL){
      if(current->pid == getpid()){
        i = current->i;
        break;
      }
      current = current->next;
    }
    workerFunction(worker_read_fds[i], worker_write_fds[i], mapSize, map, fp, folderName);
  }


  //FREE AND CLOSE ALL
  for (i = 0; i < numOfWorkers; i++) {
    close(jExecutor_read_fds[i]);
    close(jExecutor_write_fds[i]);
    close(worker_read_fds[i]);
    close(worker_write_fds[i]);
  }
  free(worker_pids);
  free(jExecutor_read_fds);
  free(jExecutor_write_fds);
  free(worker_read_fds);
  free(worker_write_fds);
  freeWorkerList(wlist);
  free(pinfo);
  for (i = 0; i < numDir; i++) {
    free(dirAr[i]);
  }
  free(dirAr);

  return 0;
}
