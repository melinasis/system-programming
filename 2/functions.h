#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct idNode* idNodePtr;
typedef struct Answer* answerPtr;

typedef struct idNode{
	int id;
	float score;
	int flag;
	idNodePtr next;
}idNode;

typedef struct Score{
	int id;
	float score;
}Score;

void dfAll(trieNodePtr root, Pinfo pinfo);
int max_tf(trieNodePtr root, char *word, char *max_path, char *max_file);
int min_tf(trieNodePtr root, char *word, char *min_path, char *min_file);
postingListNodePtr searchWord(trieNodePtr root, char* word);
postingListNodePtr searchDown(trieNodePtr current, char* word, int j);
void dfWord(trieNodePtr root, char* word);
void search(trieNodePtr root, nodePtr searchList, docPtr* docAr, answerPtr answerList, char* log_buf);
void pushIdList(idNodePtr idList, int id, int no, docPtr* docAr, int df);
int idListSize(idNodePtr idList);
void deleteIdList(idNodePtr head);
