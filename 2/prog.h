#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Dir* dirPtr;
typedef struct node* nodePtr; 						//pointer to node
typedef struct Doc* docPtr;

typedef struct Doc{
  int wordCount;
  int lines;
  int bytes;
  char path[200];
  char fileName[100];
	nodePtr wordList;
}Doc;

typedef struct Dir{
  char path[200];
}Dir;

typedef struct Prog* Pinfo;
typedef struct Prog{
	int k, numDir, numWords, w;
	char docfile[20];
}Prog;


typedef struct node{
	char word[100];
	nodePtr next;
}node;

int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
void firstReadDocfile(Pinfo pinfo);
void secondReadDocfile(Pinfo pinfo, dirPtr* dirAr);
void deleteSearchList(nodePtr head);
void commands();
nodePtr createWordList();
void freeWordList(nodePtr head);
void push(nodePtr head, char* word);
nodePtr nextNode(nodePtr current);
void printList_forSearch(nodePtr head, nodePtr searchList);

// void printList(nodePtr head);
// int countNode(nodePtr head);
// void commands(trieNodePtr root, docPtr* docAr, Pinfo pinfo);
