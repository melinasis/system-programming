#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "trie.h"
#include "postingList.h"
#include "functions.h"

int main(int argc, char *argv[]){
  int i;

  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  FILE *dfResults = fopen("dfResults.txt", "w" );
  pinfo->dfResults = dfResults;

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }

  // printf("%s\n", pinfo->docfile);
  // printf("%d\n", pinfo->k);

  if(firstRead(pinfo) == EXIT_FAILURE){
    printf("Check Id Numbers in your docfile\n");
    return EXIT_FAILURE;
  }


  int numDocs = pinfo->numDocs;
  //docAr holds all documents from input
  docPtr* docAr = malloc(numDocs*sizeof(docPtr));
  if(docAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  secondRead(pinfo, docAr);

  // //print
  pinfo->numWords = 0;
  for(i = 0; i < numDocs; i++){
    // printf("%d: ", docAr[i]->id);
    // printf("[%d words] ", docAr[i]->wordCount);
    pinfo->numWords += docAr[i]->wordCount;
    // printList(docAr[i]->wordList);
  }


  trieNodePtr root = createTrie();
  insertTrie(root, docAr, pinfo);

  printf("Index created!\n" );
  printf("Docs are %d\n", pinfo->numDocs);
  printf("Total number of words is %d\n", pinfo->numWords);
  // printPostingLists(root, 0);

  commands(root, docAr, pinfo);

  //exit

  //free map
  for(i = 0; i < numDocs; i++){
    freeWordList(docAr[i]->wordList);
    free(docAr[i]);
  }
  free(docAr);
  //

  freeTrie(root);
  free(pinfo);
  fclose(dfResults);
  return 0;
}
