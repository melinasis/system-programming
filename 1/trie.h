#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct trieNode* trieNodePtr;
typedef struct childNode* childNodePtr;
typedef struct postingListNode* postingListNodePtr;


typedef struct childNode{
  trieNodePtr myChild;

  //my brothers
  childNodePtr prev;
  childNodePtr next;

}childNode;


typedef struct trieNode{
	char value[1];

  //parent
  trieNodePtr parent;

  //my children
  childNodePtr headChild;


  //my posting list
  postingListNodePtr myPostingList;

}trieNode;


childNodePtr createChildrenList();
trieNodePtr createTrie();
void insertTrie(trieNodePtr root, docPtr* docAr, Pinfo pinfo);
void goDown(trieNodePtr current, char* word, int j, int id);
trieNodePtr existsInList(trieNodePtr current, char ch);
childNodePtr pushChildrenList(childNodePtr head, char ch, trieNodePtr parent);
void deleteDown(childNodePtr currentChild);
void freeTrie(trieNodePtr root);
void deleteTrieNode(trieNodePtr node);
