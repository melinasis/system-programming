#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct trieNode* trieNodePtr;
typedef struct Doc* docPtr;
typedef struct node* nodePtr; 						//pointer to node

typedef struct Doc{
  int id;
  int wordCount;
	nodePtr wordList;
}Doc;

typedef struct Prog* Pinfo;
typedef struct Prog{
  FILE* dfResults;
	int k, numDocs, numWords;
	char docfile[20];
}Prog;


typedef struct node{
	char word[100];
	nodePtr next;
}node;

int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
int firstRead(Pinfo pinfo);
void secondRead(Pinfo pinfo, docPtr* docAr);
nodePtr createWordList();
void push(nodePtr head, char* word);
void printList(nodePtr head);
nodePtr nextNode(nodePtr current);
int countNode(nodePtr head);
void freeWordList(nodePtr head);
void commands(trieNodePtr root, docPtr* docAr, Pinfo pinfo);
void deleteSearchList(nodePtr head);
void printList_forSearch(nodePtr head, nodePtr searchList);
