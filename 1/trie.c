#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "trie.h"
#include "postingList.h"

trieNodePtr createTrie(){
  //create root of trie
  trieNodePtr root = malloc(sizeof(trieNode));
  root->parent = NULL;
  root->myPostingList = NULL;
  strcpy(root->value, " "); //empty
  //headChild is the first node of the dynamic list keeping all the children of the trieNode
  root->headChild = createChildrenList();
  return root;
}

childNodePtr createChildrenList(){
  //dynamic list for all the children
  childNodePtr headChild = malloc(sizeof(childNode));
  headChild->myChild = NULL;
  headChild->prev = NULL;
  headChild->next = NULL;
  // headChild->postingList = NULL;

  return headChild;
}

void insertTrie(trieNodePtr root, docPtr* docAr, Pinfo pinfo){
  int i, j = 0;
  int numDocs = pinfo->numDocs;
  char ch;

  for(i = 0; i < numDocs; i++){ //for each doc
    nodePtr current = docAr[i]->wordList;
    while(current != NULL){ //for each word

      // printf("%s ready to be inserted\n", current->word);
      goDown(root, current->word, j, i /*doc id*/);
      current = nextNode(current);
    }
  }
}

void goDown(trieNodePtr current, char* word, int j, int id){
  trieNodePtr new;
  char ch = word[j];
  // printf("%c\n", ch);
  if(ch == '\0'){
    current->myPostingList = insertPostingList(current->myPostingList, id);
    // printf("END\n" );
    return;
  }
  if(current->headChild->myChild == NULL){
    //no children yet

    current->headChild->myChild = malloc(sizeof(trieNode));
    //a new trieNode has been  created
    current->headChild->myChild->parent = current;
    new = current->headChild->myChild;
    memset(new->value, '\0', sizeof(new->value));
    new->myPostingList = NULL;
    new->value[0] = ch;
    new->headChild = createChildrenList();
    // printf("%c has been entered\n", ch);
    current = new;
    j++;
    goDown(current, word, j, id);
  }
  else{
    //this trieNode has children
    trieNodePtr exists = existsInList(current, ch);
    if(exists != NULL){
      //This character already exists
      j++;
      goDown(exists, word, j, id);
    }
    else{
      //This character doesn't exist
      // printf("%c must be entered\n", ch);
      current->headChild = pushChildrenList(current->headChild, ch, current);

      j++;
      goDown(current->headChild->myChild, word, j, id);
    }
  }
}

trieNodePtr existsInList(trieNodePtr current, char ch){
  childNodePtr currentChild;

  currentChild = current->headChild;
  while(currentChild != NULL){
    if(currentChild->myChild->value[0] == ch){
      // printf("This character already exists\n");
      return currentChild->myChild;
    }
    else{
      currentChild = currentChild->next;
    }
  }
  // printf("This character doesn't exist\n" );
  return NULL;
}

childNodePtr pushChildrenList(childNodePtr head, char ch, trieNodePtr parent){
  childNodePtr new = malloc(sizeof(childNode));
  new->prev = NULL;
  new->next = head;
  head->prev = new;

  new->myChild = malloc(sizeof(trieNode));
  //a new trieNode has been  created
  new->myChild->myPostingList = NULL;
  new->myChild->parent = parent;
  memset(new->myChild->value, '\0', sizeof(new->myChild->value));
  new->myChild->value[0] = ch;
  new->myChild->headChild = createChildrenList();

  // printf("%c has been entered\n", ch);
  return new;
}

void freeTrie(trieNodePtr root){
  trieNodePtr currentTrieNode = root;
  childNodePtr currentChild = root->headChild;
  while(currentChild != NULL) {
    deleteDown(currentChild);
    currentChild->myChild = NULL;
    currentChild = currentChild ->next;
  }
  deleteTrieNode(root);
  // printf("Delete root\n");
}

void deleteDown(childNodePtr currentChild){
  trieNodePtr currentTrieNode = currentChild->myChild;
  if(currentTrieNode->myPostingList != NULL){
    deletePostingList(currentTrieNode->myPostingList);
    // printf("Posting list of %c is deleted\n", currentTrieNode->value[0]);
    currentTrieNode->myPostingList = NULL;
    if(currentTrieNode->headChild->myChild != NULL){
      //There is more
    }
    else{
      // printf("Delete trie node with value %c\n", currentTrieNode->value[0]);
      deleteTrieNode(currentTrieNode);
      return; //go up
    }
  }
  currentChild = currentTrieNode->headChild;
  while(currentChild != NULL) {
    deleteDown(currentChild);
    currentChild = currentChild->next;
    if(currentChild == NULL){
      // printf("Delete trie node with value %c\n", currentTrieNode->value[0]);
      deleteTrieNode(currentTrieNode);
    }
  }
}

void deleteTrieNode(trieNodePtr node){
  deleteChildrenList(node->headChild);
  free(node);
}

void deleteChildrenList(childNodePtr head){
  childNodePtr temp;
  childNodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
