#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "prog.h"
#include "trie.h"
#include "postingList.h"
#include "functions.h"

void dfWord(trieNodePtr root, char* word){
  postingListNodePtr postingList = searchWord(root, word);
  if(postingList != NULL){
    //word exists
    printf("%s", word);
    printPostingListSize(postingList, 0, NULL);
  }
  else{
    printf("\033[22;31mThe word you are searching for does not exist\033[m\n");
  }
}

void dfAll(trieNodePtr root, Pinfo pinfo){
  printPostingLists(root, 1, pinfo);
}

void tf(trieNodePtr root, int id, char* word){
  postingListNodePtr postingList = searchWord(root, word);
  if(postingList != NULL){
    //word exists
    //search postingList for this id
    postingListNodePtr current = postingList;
    while(current != NULL){
      if(current->id == id){
        printf("%d %s %d\n",id, word, current->no);
        return;
      }
      current = current->next;
    }
    printf("\033[22;31mThe word '%s' doesn't exist in the document given. Try different document id.\033[m\n", word );
  }
  else{
    printf("\033[22;31mThe word you are searching for does not exist\033[m\n");
  }
}

postingListNodePtr searchWord(trieNodePtr root, char* word){
  int j = 0;
  // printf("%s ready to be searched\n", word);
  return searchDown(root, word, j);
}

postingListNodePtr searchDown(trieNodePtr current, char* word, int j){
  char ch;
  ch = word[j];
  // printf("%c\n", ch);
  if(current->headChild->myChild == NULL){
    //no children
    return NULL;
  }
  else{
    //this trieNode has children
    trieNodePtr exists = existsInList(current, ch);
    if(exists != NULL){
      //This character already exists
      j++;
      if(word[j] == '\0'){
        // printf("This is the end of the word\n" );
        //Did you find it?
        if(exists->myPostingList != NULL){
          // printf("Found!!!\n" );
          return exists->myPostingList;
        }
        else{
          return NULL;
        }
      }
      searchDown(exists, word, j);
    }
    else{
      //This character doesn't exist
      return NULL;
    }
  }
}

void search(trieNodePtr root, nodePtr searchList, docPtr* docAr, Pinfo pinfo){
  int i, df, old_id, new_id;
  int at_least_one = 0;
  float old_score, new_score;
  int done = -1, counter = 1;;
  idNodePtr cur;
  idNodePtr idList = malloc(sizeof(idNode));
  idList->id = -1;
  idList->score = 0;
  idList->flag = 0;
  idList->next = NULL;

  nodePtr currentWord = searchList;
  while(currentWord != NULL){
    postingListNodePtr postingList = searchWord(root, currentWord->word);

    if(postingList != NULL){
      at_least_one = 1;
      //word exists
      // printf("%s found\n", currentWord->word);
      df = postingListSize(postingList);
      postingListNodePtr current = postingList;
      while(current != NULL){
        // printf("in doc %d\n", current->id);
        pushIdList(idList, current->id, current->no, pinfo, docAr, df);
        current = current->next;
      }
    }
    else{
      // printf("%s does not exist\n", currentWord->word);
    }
    //next word
    currentWord = currentWord->next;
  }

  if(at_least_one == 0){
    free(idList);
    printf("\033[22;31mNo results found\033[m\n" );
    return;
  }
  //print id List
  // cur = idList;
  // while(cur != NULL){
  //   printf("%d -> ", cur->id);
  //   cur = cur->next;
  // }printf("\n" );

  // printf("relevant documents = %d\n", idListSize(idList));
  int relevant = idListSize(idList);
  //array of k ids
  for(i = 0; i < pinfo->k; i++){
    cur = idList;
    old_id = -1;
    while(cur != NULL){//for each relevant document
      if(cur->id == done && done != -1){
        cur->flag = 1;
        done = -1;
      }
      if(cur->flag == 0){
        if(old_id == -1){
          old_id = cur->id;
          old_score = cur->score;
        }
        new_score = cur->score;
        new_id = cur->id;

        if(new_score > old_score){
          old_score = new_score;
          old_id = new_id;
        }
      }
      cur = cur->next;
    }
    // printf("max score at the moment = %f [%d]\n", old_score, old_id);
    //Print document
    printf("%d. (%d) [%.4f] ", counter, old_id, old_score);
    printList_forSearch(docAr[old_id]->wordList, searchList);
    done = old_id;
    counter++;
    if(counter > relevant){
      break;
    }
  }
  deleteIdList(idList);
}

void pushIdList(idNodePtr idList, int id, int no, Pinfo pinfo, docPtr* docAr, int df){
  idNodePtr current = idList;

  //Calculate score for this word and id
  int tf = no;
  float k1 = 1.2;
  float b = 0.75;
  int D = docAr[id]->wordCount;
  float avgdl = pinfo->numWords/pinfo->numDocs;
  double idf = log((pinfo->numWords - df + 0.5) / (df+0.5));
  float score = idf * ( (tf*(k1+1)) / (tf+k1*(1-b+b*(D/avgdl))) );
  // printf("score = %f\n", score);

  if(idList->id == -1){
    idList->id = id;
    idList->score += score;
  }
  else{
    while(current != NULL){
      if(current->id == id){
        current->score += score;
        return;
        //no need to enter the same id
      }
      if(current->next != NULL){
        current = current->next;
      }
      else  break;
    }
    //current is last
    // printf("%d id hasn't been entered before\n", id);
    current->next = malloc(sizeof(idNode));
    current->next->id = id;
    current->next->score = score;
    current->next->flag = 0;
    current->next->next = NULL;
  }
}

int idListSize(idNodePtr idList){
  int size = 0;
  idNodePtr current = idList;
  while(current != NULL){
    size++;
    current = current->next;
  }
  return size;
}

void deleteIdList(idNodePtr head){
  idNodePtr temp;
  idNodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
