#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct idNode* idNodePtr;

typedef struct idNode{
	int id;
	float score;
	int flag;
	idNodePtr next;
}idNode;

typedef struct Score{
	int id;
	float score;
}Score;

void dfAll(trieNodePtr root, Pinfo pinfo);
void tf(trieNodePtr root, int id, char* word);
postingListNodePtr searchWord(trieNodePtr root, char* word);
postingListNodePtr searchDown(trieNodePtr current, char* word, int j);
void dfWord(trieNodePtr root, char* word);
void search(trieNodePtr root, nodePtr searchList, docPtr* docAr, Pinfo pinfo);
void pushIdList(idNodePtr idList, int id, int no, Pinfo pinfo, docPtr* docAr, int df);
int idListSize(idNodePtr idList);
void deleteIdList(idNodePtr head);
