#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog.h"
#include "trie.h"
#include "functions.h"

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-i\033[m [filename]  input docfile\n\
\033[22;33m-k\033[m  [int]  preferred number of results from query\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1;
    int k = 10; //default
    char docfile[20];

    //initialize pinfo
    memset(pinfo->docfile,'\0',sizeof(pinfo->docfile));
    pinfo->k = k;

    memset(docfile,'\0',sizeof(docfile));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'i':
                       argIter++;
                       strcpy(docfile, argv[argIter]);
                       break;
                  case 'k':
                       argIter++;
                       k = atoi(argv[argIter]);
                       if(k == 0){
                         k = 10; //if 0 is given make k default
                       }
                       break;
                  default:
                      return EXIT_FAILURE;
              }
          }
          else{
            return EXIT_FAILURE;
          }
      }
      else{
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->docfile, docfile, sizeof(docfile));
    // printf("fd %s\n", pinfo->fd);

    pinfo->k = k;

    if(strlen(pinfo->docfile) == 0){

      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int firstRead(Pinfo pinfo){
  int numDocs;
  int idCheck = 0;  //ids should start from 0

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *pch;

  stream = fopen(pinfo->docfile, "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    line[read - 1] = 0;

    pch = strtok(line, "\t");
    // printf("%s\n", pch);

    // printf("id given is %d\n", atoi(pch));
    //check if ids are given right
    if(idCheck == atoi(pch)){
      // printf("Id %d ok\n", atoi(pch));
      idCheck++;
    }
    else{
      return EXIT_FAILURE;
    }
    numDocs = atoi(pch) + 1;  //id starts with 0
  }

  pinfo->numDocs = numDocs;

  // printf("Docs are %d\n", pinfo->numDocs);

  fclose(stream);
  free(line);

  return EXIT_SUCCESS;
}

void secondRead(Pinfo pinfo, docPtr* docAr){
  int i, j, flag;
  int position = 0;
  int wordCount = 0;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->docfile, "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char* pch;

      docPtr docP = malloc(sizeof(Doc));
      if(docP == NULL){
            printf("Error! memory not allocated.");
            return;
      }

      docP->wordList = createWordList();

      pch = strtok(line, "\t");
      docP->id = atoi(pch);

      while( (pch = strtok(NULL, " ")) != NULL){
          push(docP->wordList, pch);
          wordCount++;
      }
      docP->wordCount = wordCount;
      wordCount = 0;
      //finished reading doc
      docAr[position] = docP;   //saved to array
      position++;
  }
  fclose(stream);
  free(line);
}

nodePtr createWordList(){
  nodePtr wordList = malloc(sizeof(node));
  if(wordList == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  memset(wordList->word,'\0',sizeof(wordList->word));
  strcpy(wordList->word," ");
  wordList->next = NULL;
  return wordList;
}

void push(nodePtr head, char* word) {
    nodePtr current = head;
    if(strcmp(head->word," ") == 0){    //first node
      memset(head->word,'\0',sizeof(head->word));
      strcpy(head->word, word);
    }
    else{
      while (current->next != NULL) {
          current = current->next;
      }
      /* now we can add two nodes */
      current->next = malloc(sizeof(node));
      if(current->next == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      memset(current->next->word,'\0',sizeof(current->next->word));
      strcpy(current->next->word, word);
      current->next->next = NULL;
    }

}

int countNode(nodePtr head){
  int count = 1;
  nodePtr current = head;

  while(current->next != NULL)
  {
    current = current->next;
    count++;
    //printf("%d\n", count);
  }
  return count;
}

void printList(nodePtr head){
  nodePtr current = head;
  printf("%s ", current->word);
  while(current->next != NULL){
    current = current->next;
    printf("%s ", current->word);
  }
  printf("\n");
  // printf("Number of nodes: %d\n", countNode(head));
}

void printList_forSearch(nodePtr head, nodePtr searchList){
  nodePtr current = head;
  nodePtr currentWord;

  while(current != NULL){
    currentWord = searchList;

    while(currentWord != NULL){
      if(strcmp(currentWord->word, current->word) == 0){
        printf("\033[22;32m%s \033[m", current->word);
        break;
      }
      currentWord = currentWord->next;
    }
    if(currentWord == NULL){
      printf("%s ", current->word);
    }

    current = current->next;
  }
  printf("\n\n");
  // printf("Number of nodes: %d\n", countNode(head));
}

nodePtr nextNode(nodePtr current){
  return current->next;
}

void freeWordList(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}

void freeWordList(nodePtr head);

void commands(trieNodePtr root, docPtr* docAr, Pinfo pinfo){

  char* str;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  printf("\033[22;33mEnter command: \033[m");

  while ((read = getline(&line, &len, stdin)) != -1){
    line[read - 1] = 0;
    char *pch;
    pch = strtok(line, " ");
    if(strcmp(pch, "\\df") == 0){
      pch = strtok(NULL, " ");

      if(pch == NULL){
        //just df
        // printf("\033[22;34mPrinting document frequency vector:\033[m\n");
        dfAll(root, pinfo);
        printf("\033[22;33mEnter command: \033[m");
      }
      else{
        //df with word
        dfWord(root, pch);
        printf("\033[22;33mEnter command: \033[m");
      }
    }
    else if(strcmp(pch, "\\tf") == 0){
      pch = strtok(NULL, " ");
      if(pch == NULL){
        printf("\033[22;31mCommand doesn't exist. Try again.\033[m\n" );
        printf("\033[22;33mEnter command: \033[m");
        continue;
      }

      int id = atoi(pch);

      pch = strtok(NULL, " ");
      if(pch == NULL){
        printf("\033[22;31mCommand doesn't exist. Try again.\033[m\n" );
        printf("\033[22;33mEnter command: \033[m");
        continue;
      }
      // printf("\033[22;34mPrinting term frequency of word '%s' in document %d\033[m \n", pch, id);
      tf(root, id, pch);
      printf("\033[22;33mEnter command: \033[m");
    }
    else if(strcmp(pch, "\\search") == 0){
      pch = strtok(NULL, " ");
      if(pch == NULL){
        printf("\033[22;31mMissing word to search. Try again\033[m\n" );
        printf("\033[22;33mEnter command: \033[m");
        continue;
      }
      //first word
      nodePtr searchList = malloc(sizeof(node));
      strcpy(searchList->word, pch);
      searchList->next = NULL;
      int sum = 1;

      nodePtr current = searchList;
      while((pch = strtok(NULL, " ")) != NULL && sum < 10){
          current->next = malloc(sizeof(node));
          current = current->next;
          strcpy(current->word, pch);
          current->next = NULL;
          sum++;
      }

      //print
      // current = searchList;
      // while(current != NULL){
      //   printf("%s -> ", current->word);
      //   current = current -> next;
      // }printf("\n");

      search(root, searchList, docAr, pinfo);

      deleteSearchList(searchList);


      //free List
      printf("\033[22;33mEnter command: \033[m");
    }
    else if(strcmp(pch, "\\exit") == 0){
      printf("GOODBYE!\n");
      free(line);
      break;
    }
    else{
      printf("\033[22;31mCommand doesn't exist. Try again.\033[m\n" );
      printf("\033[22;33mEnter command: \033[m");
    }
  }
}

void deleteSearchList(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
